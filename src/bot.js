const dialogflow = require("dialogflow");

let projectId = "laika-debubg";
const languageCode = "en-Es";

function sendMessageToDialogflow(sessionId, messageText) {
    const sessionClient = new dialogflow.SessionsClient();
    const sessionPath = sessionClient.sessionPath(projectId, sessionId);

    const request = {
        session: sessionPath,
        queryInput: {
            text: {
                text: messageText,
                languageCode: languageCode
            }
        }
    };

    let response = sessionClient.detectIntent(request);
    return response;
}

module.exports = sendMessageToDialogflow;