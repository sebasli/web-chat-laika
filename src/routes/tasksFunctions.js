const logger = require('../../config/winston');
const jwt = require("jsonwebtoken");

const secretToken = process.env.TOKENJWTAGT;
function NOW() {
    var date = new Date();
    var aaaa = date.getFullYear();
    var gg = date.getDate();
    var mm = date.getMonth() + 1;
    if (gg < 10) gg = "0" + gg;
    if (mm < 10) mm = "0" + mm;
    var cur_day = aaaa + "-" + mm + "-" + gg;
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    if (hours < 10) hours = "0" + hours;
    if (minutes < 10) minutes = "0" + minutes;
    if (seconds < 10) seconds = "0" + seconds;
    return cur_day + " " + hours + ":" + minutes + ":" + seconds;
}


async function callUserWhatsapp(tokenUser, idUser) {
    return await global.db.one(
        "SELECT cg.name nombre_grupo FROM chat_group cg INNER JOIN chat_permission cp ON cg.id = cp.chat_group_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
        ["2", tokenUser]
    )
        .then(async () => {
            return await global.db.one("SELECT id from public.chat_user WHERE accountwa=$1;", [
                idUser
            ])
                .then(data => {
                    return data;
                })
                .catch(error => {
                    console.log('TASK_FUNCTIONS_CALL_USER_WHATSAPP_02');
                    logger.error(error.message);
                    return null;
                });
        })
        .catch((err) => {
            console.log('TASK_FUNCTIONS_CALL_USER_WHATSAPP_01');
            logger.error(err);
            return null;
        });
}

async function reloadUserWa(tokenUser, name, id) {
    return await global.db.one(
        "SELECT cg.name nombre_grupo FROM chat_group cg INNER JOIN chat_permission cp ON cg.id = cp.chat_group_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
        ["2", tokenUser]
    )
        .then(() => {
            const token = jwt.sign(
                { name, id },
                secretToken,
                { expiresIn: "14h" }
            );
            return {
                token
            }
        })
        .catch(err => {
            console.log('TASK_FUNCTIONS_RELOAD_USER_WA');
            logger.error(err);
            throw err;
        });
}

async function saveUser(name, email, conditions, acount, accountMessenger) {
    const token = jwt.sign(
        { name, email },
        secretToken,
        { expiresIn: "14h" }
    );
    return await global.db.any(
        "INSERT INTO chat_user (name, email, fecha, conditions, accountwa, accountmessenger) VALUES ($1, $2, $3, $4, $5, $6) RETURNING (id)",
        [name, email, NOW(), conditions, acount, accountMessenger]
    )
        .then(data => {
            const result = { data: data, token: token };
            return result;
        })
        .catch(error => {
            console.log('TASK_FUNCTIONS_SAVE_USER');
            logger.error(error.message);
            throw error;
        });
}

async function saveMessageAgent(token, idUser, idSocket, message, from) {
    if (typeof token !== "undefined") {
        jwt.verify(token, secretToken, async err => {
            if (err) {
                console.error("TASK_FUNCTIONS_SAVE_MESSAGE_AGENT_01");
                logger.error(err.message);
                return null;
            } else {
                return await global.db.query(
                    "INSERT INTO chat_message_bot_agent (id_chat_user, id_socket, message, fecha, who) VALUES ($1, $2, $3, $4, $5)",
                    [
                        idUser,
                        idSocket,
                        message,
                        NOW(),
                        from
                    ]
                )
                    .then(() => {
                        return "Succesful";
                    })
                    .catch(async function (error) {
                        console.error("TASK_FUNCTIONS_SAVE_MESSAGE_AGENT_02");
                        logger.error(error.message);
                        return null;
                    });
            }
        });
    } else {
        console.error('TASK_FUNCTIONS_SAVE_MESSAGE_AGENT_03');
        logger.error('JWT must be provided');
        return null;
    }
}

async function saveMessage(token, idUser, message, from) {
    if (typeof token !== "undefined") {
        jwt.verify(token, secretToken, async err => {
            if (err) {
                console.error("TASKS_FUNCTIONS_SAVE_MESSAGE_01");
                logger.error(err);
                throw err;
            } else {
                return await global.db.query(
                    "INSERT INTO chat_message (id_chat_user, message, fecha, who) VALUES ($1, $2, $3, $4)",
                    [idUser, message, NOW(), from]
                )
                    .then(() => {
                        return "success";
                    })
                    .catch(error => {
                        console.error('TASKS_FUNCTIONS_SAVE_MESSAGE_02');
                        logger.error(error.message);
                        throw error;
                    });
            }
        });
    } else {
        throw new Error("JWT must be provided");
    }
}

async function isDayHabil(fecha) {
    if (fecha) {
        return await global.db.any(
            "SELECT cal.es_habil, cal.dia_semana, (case when cal.es_habil=true then hor.dias_habiles_inicio else hor.dias_no_habiles_inicio end) as hourStart, (case when cal.es_habil=true then hor.dias_habiles_fin else hor.dias_no_habiles_fin end) as hourEnd FROM public.chat_calendario as cal, public.chat_horario_atencion as hor WHERE cal.dia=$1;",
            [fecha]
        )
            .then(data => {
                return data;
            })
            .catch(async function (error) {
                console.error('TASKS_FUNCTIONS_IS_DAY_HABIL_01');
                logger.error(error.message);
                throw error;
            });
    } else {
        throw new Error("You have to send a date for call if is a day normal");
    }
}

async function tmo() {
    return await global.db.any("SELECT * FROM public.chat_tmo")
        .then(data => {
            return data;
        })
        .catch(async function (error) {
            console.error('TASKS_FUNCTIONS_TMO_01');
            logger.error(error.message);
            throw error;
        });
}

async function templates() {
    return await global.db.any("SELECT * FROM public.chat_plantillas")
        .then(data => {
            return data;
        })
        .catch(async function (error) {
            console.error('TASKS_FUNCTIONS_PLANTILLAS_01');
            logger.error(error.message);
            throw error;
        });
}

module.exports = {
    callUserWhatsapp,
    reloadUserWa,
    saveUser,
    saveMessageAgent,
    saveMessage,
    isDayHabil,
    tmo,
    templates
}