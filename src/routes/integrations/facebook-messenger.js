const express = require('express');
const router = express.Router();

const axios = require('axios');
var { Timer } = require("easytimer.js"); // Timer para gestón tiempo de agentes
const logger = require('../../../config/winston');
require("dotenv").config();


/** Variables */
const pathUrlTask = process.env.PATH_URL_SERVER;
const tokenAmd = process.env.TOKEN_AMD;
global.usersOfMessenger = [];

/*
 Función para buscar indice en array de agentes, con menos conversaciones
*/
function findMinCon(arr) {
    let min = arr[0].numCon,
        ind = 0,
        v;
    for (let i = 1, len = arr.length; i < len; i++) {
        v = arr[i].numCon;
        v < min ? ((min = v), (ind = i)) : ((min = min), ind);
    }
    return ind;
}


/*
  Funcion para obtener formato de año-mes-dia yyyy:mm:dd
*/
function nowDate() {
    var date = new Date();
    var aaaa = date.getFullYear();
    var gg = date.getDate();
    var mm = date.getMonth() + 1;
    if (gg < 10) gg = "0" + gg;
    if (mm < 10) mm = "0" + mm;
    var cur_day = aaaa + "-" + mm + "-" + gg;
    return cur_day;
}
/*
  Funcion para llamada de horario de atención del día
*/
function isHabilDay() {
    return axios
        .post(pathUrlTask + "/isDayHabil", { fecha: nowDate() })
        .then(function(data) {
            return data.data[0];
        })
        .catch(e => {
            logger.error(
                "Error, no existe propiedad numCon en array de objetos global.agentOnline: " +
                e
            );
            return null;
        });
}

/*
  Funcion para convertir formato de hora-minutos-segundos hh:mm:ss a segundos
*/
function convertToInt(num) {
    try {
        let arr = num.split(":");
        let hr = parseInt(arr[0]);
        let min = parseInt(arr[1]);
        let seg = parseInt(arr[2]);
        let result = hr * 3600 + min * 60 + seg;
        return result;
    } catch {
        logger.error("Error, no existe array para hacerle split");
    }
}

/*
  Funcion para obtener formato de hora-minutos-segundos hh:mm:ss
*/
function nowHour() {
    var date = new Date();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    if (hours < 10) hours = "0" + hours;
    if (minutes < 10) minutes = "0" + minutes;
    if (seconds < 10) seconds = "0" + seconds;
    return hours + ":" + minutes + ":" + seconds;
}

function conversationsExistsAndIsActive(senderId) {
    const result = global.usersOfMessenger.find(item => {
        return item.idSocket === senderId;
    });

    if (result) {
        return true;
    } else {
        return false;
    }
}

async function getUserInfo(userId) {
    try {
        const userInfo = await axios.get(`https://graph.facebook.com/v6.0/${userId}?fields=first_name,last_name,profile_pic&access_token=${process.env.PAGE_ACCESS_TOKEN}`)
        return userInfo.data;
    } catch (error) {
        logger.error(error);
    }
}

async function checkConversation(customerInfo) {
    const nameCustomerMessenger = `${customerInfo.first_name} ${customerInfo.last_name}`;
    await axios
        .post(pathUrlTask + "/callUserMessenger", {
            tokenUser: tokenAmd,
            idUser: customerInfo.id
        })
        .then(async function(resp) {
            console.log(resp.data);
            if (resp.data) {
                let idUs = resp.data.id;
                await axios
                    .post(pathUrlTask + "/reloadUserMessenger", {
                        tokenUser: tokenAmd,
                        id: customerInfo.id,
                        name: nameCustomerMessenger
                    })
                    .then(function(resp) {
                        global.usersOfMessenger.push({
                            id: idUs,
                            name: nameCustomerMessenger,
                            idSocket: customerInfo.id,
                            idCon: 0,
                            jwt: resp.data.token,
                            bot: false,
                            timeChat: new Timer(),
                            timeChatFin: new Timer()
                        });
                    });
            } else {
                const requestBody = {
                    name: nameCustomerMessenger,
                    accountMessenger: customerInfo.id
                }
                await axios.post(pathUrlTask + "/saveUser", requestBody)
                    .then(async resp => {
                        global.usersOfMessenger.push({
                            id: resp.data[0].id,
                            name: nameCustomerMessenger,
                            idSocket: senderId,
                            idCon: 0,
                            jwt: resp.data.token,
                            bot: false,
                            timeChat: new Timer(),
                            timeChatFin: new Timer()
                        })
                        return "seguir";
                    }).catch(error => {
                        logger.error(error.response.data.error)
                    });
            }
        })
        .catch(e => {
            logger.error("aca:" + e);
            return "seguir";
        });
    return "seguir";
}

async function asignAgentFacebookMessenger(authorMess, payload) {
    let templates = await tasksFunctions.templates();
    let plant = [];
    templates.forEach((e) => {
        if (e.id === 3) {
            plant.push(e)
        }
    })
    var hour = convertToInt(nowHour());
    if (hour >= global.timeStart && hour <= global.timeStop) {
        var result = global.agentOnline.filter(
            soc =>
            soc.state === "ONLINE" &&
            soc.connect === true &&
            soc.numCon < soc.capacity
        );
        if (result === undefined || result.length === 0) {
            var author = authorMess;
            var text = plant[0].valor;
            global.chatsPendientes.push(payload);
            setTimeout(function() {
                sendMessage(authorMess, {
                    "text": text
                })
            }, 2500);
        } else {
            try {
                let x = findMinCon(result);
                global.io.sockets.in(result[x].idAgent).emit("event", payload, "Facebook Messenger");
                let pos = global.agentOnline.findIndex(ele => ele.idAgent === result[x].idAgent);
                agentOnline[pos].numCon += 1;
            } catch (e) {
                logger.error("Error asignando conversacion al agente: " + e);
            }
        }
    } else {
        let pos = global.usersOfMessenger.findIndex(el => el.idSocket === senderId);
        global.usersOfMessenger[pos].bot = false;
        var text = plant[0].valor;
        setTimeout(function() {
            sendMessage(authorMess, {
                "text": text
            })
        }, 2500);
    }
}

async function sendMessage(senderId, message) {
    try {
        await axios.post("https://chatbots.montechelo.com.co/widget/laika/integrations/facebook-messenger/send-message", {
            sender: {
                id: senderId
            },
            message
        });
        logger.info("Facebook Messenger Message Sended");
    } catch (error) {
        logger.error(error);
    }
}

/** Routes */
router.get("/webhook", (req, res) => {
    let VERIFY_TOKEN = 'ZJDw5xmPKr';

    let mode = req.query["hub.mode"];
    let token = req.query["hub.verify_token"];
    let challenge = req.query["hub.challenge"];

    if (mode && token) {
        if (mode === "subscribe" && token === VERIFY_TOKEN) {
            logger.info("WEBHOOK_VERIFIED");
            res.status(200).send(challenge);
        } else {
            res.sendStatus(403);
        }
    }
});

router.post("/webhook", (req, res) => {
    let body = req.body;

    if (body.object === "page") {
        body.entry.forEach(async function(entry) {
            let message = entry.messaging[0];
            console.log(message);
            /**
             * message = {
             *  sender: {id},
             *  recipient: {id},
             *  timestamp: number,
             *  message: {mid, text}
             * }
             */
            // Valida que exista el mensaje, normalmente si falla es por que el agente envio un mensaje.
            try {
                if (!message.sender.id || !message.message || message.message.app_id) {
                    return;
                }
            } catch (error) {
                return;
            }

            const senderId = message.sender.id;
            const textMessage = [];

            if (message.message.text && message.sender.id) {
                textMessage.push(message.message.text);
            } else if (message.message.attachments) {
                message.message.attachments.forEach(element => {
                    // if (element.type == "file") {
                    // } else if (element.type == "image") {
                    // }
                    textMessage.push(element.payload.url);
                });
            }

            var conversationExists = conversationsExistsAndIsActive(message.sender.id);
            if (!conversationExists) {
                const customerInfo = await getUserInfo(senderId);
                console.log(customerInfo);
                await checkConversation(customerInfo);
            }
            let pos = global.usersOfMessenger.findIndex(el => el.idSocket == senderId);
            if (pos >= 0 && global.usersOfMessenger[pos].bot === false) {
                textMessage.forEach(element => {
                    axios.post(
                        pathUrlTask + "/saveMessage", {
                            idUser: global.usersOfMessenger[pos].id,
                            message: element,
                            from: "user"
                        }, {
                            headers: {
                                "Content-Type": "application/json",
                                authorization: "Bearer " + global.usersOfMessenger[pos].jwt
                            }
                        }
                    );
                });
                /*var sessionId = "{from}".format(message);
                var messages = message.body;
                const obj = await detectTextIntent(
                  projectId,
                  sessionId,
                  messages,
                  languageCode
                );
                var actionQuery = obj[0].queryResult.action; */
                var actionQuery = "menuc";

                /** Asignación de agente desde Facebook Messenger */
                if (actionQuery == "menuc" || actionQuery == "menud") {
                    if (global.timeStart) {
                        global.usersOfMessenger[pos].bot = true;
                        asignAgentFacebookMessenger(senderId, {
                            id: global.usersOfMessenger[pos].id,
                            socketId: senderId,
                            channel: "Facebook Messenger",
                        });
                    } else {
                        isHabilDay().then(data => {
                            timeStart = data.hourstart;
                            timeStop = data.hourend;
                            logger.info(
                                "Horario de atención del día de hoy inicia;" +
                                timeStart +
                                " finalzia: " +
                                timeStop
                            );
                            global.usersOfMessenger[pos].bot = true;
                            asignAgentFacebookMessenger(senderId, {
                                id: global.usersOfMessenger[pos].id,
                                socketId: senderId,
                                channel: "Facebook Messenger",
                            });
                        });
                    }
                } else {
                    try {
                        var mensajes = obj[0].queryResult.fulfillmentMessages;
                        mensajes.forEach(function(e) {
                            if (e.platform === "PLATFORM_UNSPECIFIED") {
                                try {
                                    let textFromBot = e.text.text[0];
                                    if (textFromBot) {
                                        axios.post(
                                            pathUrlTask + "/saveMessage", {
                                                idUser: global.usersOfMessenger[pos].id,
                                                message: textFromBot,
                                                from: "bot"
                                            }, {
                                                headers: {
                                                    "Content-Type": "application/json",
                                                    authorization: "Bearer " + global.usersOfMessenger[pos].jwt
                                                }
                                            }
                                        );
                                    }
                                    sendMessage(senderId, {
                                        "text": textFromBot
                                    })
                                } catch (err) {
                                    logger.error(err);
                                }
                            }
                        });
                    } catch (err) {
                        logger.error("Error, no hay resultado del API de Dialogflow");
                    }
                }

                /** Plataforma BotHUman */
            } else {
                global.usersOfMessenger[pos].timeChat.reset();
                global.usersOfMessenger[pos].timeChatFin.stop();
                textMessage.forEach(element => {
                    if (element && element.toLowerCase().includes("hablar con monty bot")) {
                        let pos = searchIndexChatPen(sockidfor);
                        if (pos !== -1) {
                            global.usersOfMessenger[pos].bot = false;
                            sendMessage(senderId, {
                                "text": "Hola nuevamente, escribe menú para continuar"
                            })
                        }
                        global.chatsPendientes.splice(pos, 1);
                    }

                    axios.post(
                        pathUrlTask + "/saveMessageAgent", {
                            idUser: global.usersOfMessenger[pos].id,
                            idSocket: global.usersOfMessenger[pos].idCon,
                            message: element,
                            from: "user"
                        }, {
                            headers: {
                                "Content-Type": "application/json",
                                authorization: "Bearer " + global.usersOfMessenger[pos].jwt
                            }
                        }
                    );
                    global.io.sockets.in(global.usersOfMessenger[pos].idSocket).emit("private-message", {
                        socketIdUser: global.usersOfMessenger[pos].idSocket,
                        messageUser: element,
                        idCon: global.usersOfMessenger[pos].idCon
                    });
                });
            }
        });

        // Returns a '200 OK' response to all requests
        res.status(200).send("EVENT_RECEIVED");
    } else {
        // Returns a '404 Not Found' if event is not from a page subscription
        res.sendStatus(404);
    }
});
/**
 * emit 
 * private-message // Agente usuario ({
 *    messageUser, 
 *    socketIdUser || room, 
 *    state 1 recien abierta 2 recien cerrada null en curso, 
 *    idCon id conversation, 
 *    channel "Messenger", 
 *    who "Agent || User", 
 *    pathFile // URL Imagen
 * })
 * messages 
 * event // asignar ({socketId, id,}, channel)
 */

router.post("/send-message", async(req, res) => {
    if (!req.body.sender.id || !req.body.message) {
        res.status(400).json({
            error: "Insufficient arguments"
        });
    }

    const senderId = req.body.sender.id;
    const message = req.body.message;

    try {
        let requestBody = {
            "messaging_type": "RESPONSE",
            "recipient": {
                "id": senderId
            },
            message
            // "message": {
            //   "text": message
            // }
        }
        console.log(requestBody)
        await axios.post(`https://graph.facebook.com/v6.0/me/messages?access_token=${process.env.PAGE_ACCESS_TOKEN}`, requestBody, {
            headers: {
                "Content-Type": "application/json"
            }
        })
        logger.info('Mensaje enviado!')
    } catch (error) {
        logger.error("No se pudo enviar el mensaje:" + error);
    }

});

module.exports = router;
module.exports.methods = {
    sendMessage
}