(
    function(window, document, scriptTagName, bodyTagName, element, scriptAddress, globalName, scriptElement, scriptElement2, otherScriptElement, bodyElement) {
        // store the name of the global variable
        window['WebChatlaika'] = globalName;

        // creates a global variable to delegate, so it will be available before script loaded 
        window[globalName] = window[globalName] || function() {
            // pushes arguments that were passed to delegate into queue
            // so they can be accessed later from the main script
            (window[globalName].q = window[globalName].q || []).push(arguments)
        };

        // epoch timestamp when the SDK was initialized
        window[globalName].l = 1 * new Date();

        // dynamically create a script tag element with async execution,
        scriptElement = document.createElement(scriptTagName);
        otherScriptElement = document.getElementsByTagName(scriptTagName)[0];
        scriptElement.async = 1; // it is just a shorter version of `true`
        scriptElement.src = scriptAddress;

        window.addEventListener('load', function() {
            scriptElement2 = document.createElement(element);
            bodyElement = document.getElementsByTagName(bodyTagName)[0];
            bodyElement.append(scriptElement2);
        })

        // inject the script tag before first script tag found within document
        otherScriptElement.parentNode.insertBefore(scriptElement, otherScriptElement);

    })
(window, document, 'script', 'BODY', 'web-icon', 'https://chatbots.montechelo.com.co/widget/laika/bundle.js', 'ga');
ga('create', 'laika', 'auto');