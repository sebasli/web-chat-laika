var express = require("express");
var app = express();
var server = require("http").Server(app);
const https = require('https');
const io = require("socket.io")(server, {
    path: "/widget/laika/socket.io"
});
global.io = io;
const path = require("path");

/** Integrations */
const facebookMessengerIntegration = require('./routes/integrations/facebook-messenger')
    /* Configurations */
const dotenv = require("dotenv");
dotenv.config(); // Acceder a variables de entorno .env

const cors = require("cors");
app.use(cors()); // Permitir que se le hagan peticiones al servidor desde diferentes puertos o ip
const logger = require("../config/winston"); // Utilidad para guardar logs
//Manda mensajes de WA
const utilMessages = require("./routes/utilities/message");

const morgan = require("morgan");
app.use(morgan("combined", { stream: logger.stream })); // Escucha las peticiones al servidor
const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: true }));
const format = require("string-format"); // Formateador de Strings
format.extend(String.prototype, {});
app.use(express.json());

/* Routes */
app.use("/widget/laika/", express.static("src/public"));
app.use("/widget/laika/", express.static("node_modules"));
app.use("/widget/laika/", express.static("build"));
app.use("/bothuman/laika", express.static("dist"));
app.use("/widget/laika/tasks", require("./routes/tasks"));
app.use("/widget/laika/integrations/facebook-messenger", facebookMessengerIntegration);

/* Utilities */
const tasksFunctions = require('./routes/tasksFunctions');

// Whatsapp
const sulla = require("@open-wa/wa-automate"); // Librería de Whatsapp
const { decryptMedia } = require("@open-wa/wa-automate");
const ev = require("@open-wa/wa-automate").ev;
const mime = require("mime-types");

// Generales
const fs = require("fs"); // Manejo de File System
var { Timer } = require("easytimer.js"); // Timer para gestón tiempo de agentes
var axios = require("axios"); // Para hacer peticiones
var schedule = require("node-schedule"); // Para programador de tareas

/* Global variables */
// Whatsapp
var clientSulla;
var qrIsScanned = false;
userWhatsapp = [];

// Guardado dinamico de estado agentes, chat pendientes y conexiones
var connections = [];
chatsPendientes = [];
var chatsFinalizados = 0;
var allMsg = 0;
agentOnline = [];
var supervisorOnline = [];
var administradorOnline = [];

// Programador de tareas
timeStart = undefined;
timeStop = undefined;
warning = undefined;
ending = undefined;

var tmo = []; // Tiempo medio de operación

const projectId = process.env.DIALOGFLOW_PROJECT_ID;
const pathUrlTask = process.env.PATH_URL_SERVER;
const tokenAmd = process.env.TOKEN_AMD;

const languageCode = "en-Es";

(
    async() => {
        await tmoActual();
    }
)();
/*
  Tarea para definir horario de atención del día, se ejecuta a las 00:01 am de la mañana
*/
var j = schedule.scheduleJob("1 0 * * *", async() => {
    chatsPendientes = [];
    userWhatsapp = [];
    tmo = [];
    chatsFinalizados = 0;
    allMsg = 0;
    const data = await isHabilDay();
    timeStart = data.hourstart;
    timeStop = data.hourend;
    logger.info(
        "Horario de atención del día de hoy inicia;" +
        timeStart +
        " finalzia: " +
        timeStop
    );
});

setInterval(function() {
    io.sockets.emit("cola_chats", {
        chatP: chatsPendientes.length,
        chatF: chatsFinalizados,
        agents: agentOnline,
        allMsg: allMsg
    });
}, 1000);

io.sockets.on("connection", async function(socket) {
    //Instancia de Timer para tiempo Online y Break
    var timerOnline = new Timer();
    var timerBreak = new Timer();
    var timeChat = new Timer();
    var timeChatFin = new Timer();

    connections.push(socket.id);


    //Suscripcion al socket de tranferencia de datos
    socket.on("transferencia", data => {
        socket.broadcast.to(data.to).emit("event", data.user, data.channel);
    });

    //Escucha desconexion de usuario
    socket.on("disconnect", function() {
        try {
            let pos = searchIndexAgentSocket(socket.id);
            if (agentOnline[pos]) {
                agentOnline[pos].connect = false;
            }
        } catch (e) {
            logger.error(
                "Error, no existe propiedad connect en array de objetos agentOnline: " +
                e
            );
        }
        timerOnline.stop();
        timerBreak.stop();
        timeChat.stop();
        timeChatFin.stop();
        connections.splice(connections.indexOf(socket.id), 1);
    });
    /*
         Para manejo de Administrador
         */
    socket.on("Administrador", function(room, payload) {
        if (payload.idSocketAgent) {
            socket.join(room);
            var result = administradorOnline.find(
                soc => soc.idAgent === payload.idSocketAgent
            );
            if (result === undefined) {
                administradorOnline.push({
                    idAgent: payload.idSocketAgent,
                    numCon: payload.numeroCon,
                    sockId: socket.id,
                    state: payload.stateAgent,
                    connect: true,
                    nameAgent: payload.nameAgent,
                    capacity: payload.capacity
                });
            }
        }
    });
    /*
       Para manejo de tiempo supervisor
       */
    socket.on("Supervisor", function(room, payload) {
        if (payload.idSocketAgent) {
            socket.join(room);
            var result = supervisorOnline.find(
                soc => soc.idAgent === payload.idSocketAgent
            );
            if (result === undefined) {
                supervisorOnline.push({
                    idAgent: payload.idSocketAgent,
                    numCon: payload.numeroCon,
                    sockId: socket.id,
                    state: payload.stateAgent,
                    connect: true,
                    nameAgent: payload.nameAgent,
                    capacity: payload.capacity
                });
                let pos = searchIndexSuperv(payload.idSocketAgent);
                if (
                    payload.stateAgent === "ONLINE" ||
                    payload.stateAgent === "OCUPADO"
                ) {
                    addEventimerOnlineSup(timerOnline, socket, pos);
                    addEvenTimeBreakSup(timerBreak, socket, pos);
                    timerOnline.start();
                } else if (payload.stateAgent === "BREAK") {
                    addEventimerOnlineSup(timerOnline, socket, pos);
                    addEvenTimeBreakSup(timerBreak, socket, pos);
                    timerBreak.start();
                }
            } else if (result.state === "ONLINE") {
                try {
                    let pos = searchIndexSuperv(payload.idSocketAgent);
                    supervisorOnline[pos].sockId = socket.id;
                    supervisorOnline[pos].connect = true;
                    addEventimerOnlineSup(timerOnline, socket, pos);
                    addEvenTimeBreakSup(timerBreak, socket, pos);
                    var valueinSeconds = convertToInt(supervisorOnline[pos].timerOnline);
                    precisionOnline(timerOnline, valueinSeconds);
                    socket.emit("timer_break", supervisorOnline[pos].timerBreak);
                } catch (e) {
                    logger.error(
                        "Error, no existe propiedad timerOnline en array de objetos agentOnline: " +
                        e
                    );
                }
            } else if (result.state === "OCUPADO") {
                try {
                    let pos = searchIndexSuperv(payload.idSocketAgent);
                    supervisorOnline[pos].sockId = socket.id;
                    supervisorOnline[pos].connect = true;
                    addEventimerOnlineSup(timerOnline, socket, pos);
                    addEvenTimeBreakSup(timerBreak, socket, pos);
                    var valueinSeconds = convertToInt(supervisorOnline[pos].timerOnline);
                    precisionOnline(timerOnline, valueinSeconds);
                    socket.emit("timer_break", supervisorOnline[pos].timerBreak);
                } catch (e) {
                    logger.error(
                        "Error, no existe propiedad timerOnline en array de objetos agentOnline: " +
                        e
                    );
                }
            } else if (result.state === "BREAK") {
                try {
                    let pos = searchIndexSuperv(payload.idSocketAgent);
                    supervisorOnline[pos].sockId = socket.id;
                    supervisorOnline[pos].connect = true;
                    addEventimerOnlineSup(timerOnline, socket, pos);
                    addEvenTimeBreakSup(timerBreak, socket, pos);
                    var valueinSeconds = convertToInt(supervisorOnline[pos].timerBreak);
                    precisionBreak(timerBreak, valueinSeconds);
                    socket.emit("timer_online", supervisorOnline[pos].timerOnline);
                } catch (e) {
                    logger.error(
                        "Error, no existe propiedad timerBreak en array de objetos agentOnline: " +
                        e
                    );
                }
            }
            //Cada vez que el usuario cambia a estado ONLINE
        } else if (payload.checkCola) {
            socket.join(room);
            let pos = searchIndexSuperv(room);
            try {
                if (payload.newStat) {
                    if (supervisorOnline[pos].timerOnline) {
                        var valueinSeconds = convertToInt(
                            supervisorOnline[pos].timerOnline
                        );
                        precisionOnline(timerOnline, valueinSeconds);
                    } else {
                        addEventimerOnlineSup(timerOnline, socket, pos);
                        timerOnline.start();
                    }
                    timerBreak.stop();
                    supervisorOnline[pos].state = payload.newStat;
                }
            } catch (e) {
                logger.error(
                    "Error, no existe propiedad State en array de objetos agentOnline: " +
                    e
                );
            }
            //Cada vez que el usuario cambia de estado
        } else if (payload.newState) {
            let pos = searchIndexSuperv(room);
            if (payload.newState != "OFFLINE") {
                try {
                    supervisorOnline[pos].connect = true;
                    supervisorOnline[pos].state = payload.newState;
                    if (payload.newState != "ONLINE") {
                        if (payload.newState == "OCUPADO") {
                            if (supervisorOnline[pos].timerOnline) {
                                var valueinSeconds = convertToInt(
                                    supervisorOnline[pos].timerOnline
                                );
                                timerBreak.stop();
                                precisionOnline(timerOnline, valueinSeconds);
                            } else {
                                addEventimerOnlineSup(timerOnline, socket, pos);
                                timerBreak.stop();
                                timerOnline.start();
                            }
                        } else if (supervisorOnline[pos].timerBreak) {
                            var valueinSeconds = convertToInt(
                                supervisorOnline[pos].timerBreak
                            );
                            timerOnline.stop();
                            precisionBreak(timerBreak, valueinSeconds);
                        } else {
                            addEvenTimeBreakSup(timerBreak, socket, pos);
                            timerOnline.stop();
                            timerBreak.start();
                        }
                    }
                } catch (e) {
                    logger.error(
                        "Error, no existe propiedad del array de objetos agentOnline: " + e
                    );
                }
            } else {
                supervisorOnline.splice(pos, 1);
                timerOnline.stop();
                timerBreak.stop();
            }
            //Cada vez que ingresa un nuevo usuario
        }
    });
    //Funcion que envia IMAGENES a WhatsApp Oficial mediante Infobip
    function sendImaggeWha(to, img) {
        axios
            .post(
                "https://yrrz31.api.infobip.com/omni/1/advanced",
                JSON.stringify({
                    scenarioKey: "070D3447E7EF0942B2F296E8CA780286",
                    destinations: [{
                        to: {
                            phoneNumber: to
                        }
                    }],
                    whatsApp: {
                        text: "Imagen Bothuman!",
                        imageUrl: img
                    }
                }), {
                    headers: {
                        "Authorization": "Basic bGFpa2Fjb2xfV2E6TTBudDNjaDNsMCo=",
                        "Content-Type": "application/json"
                    }
                }
            )
            .then(data => {
                return true;
            })
            .catch(err => {
                return false;
            });
    }
    //Funcion que envia ARCHIVOS a WhatsApp Oficial mediante Infobip
    function sendFyleWha(to, file) {
        axios
            .post(
                "https://yrrz31.api.infobip.com/omni/1/advanced",
                JSON.stringify({
                    scenarioKey: "070D3447E7EF0942B2F296E8CA780286",
                    destinations: [{
                        to: {
                            phoneNumber: to
                        }
                    }],
                    whatsApp: {
                        text: "ARCHIVO",
                        fileUrl: file
                    }
                }), {
                    headers: {
                        "Authorization": "Basic bGFpa2Fjb2xfV2E6TTBudDNjaDNsMCo=",
                        "Content-Type": "application/json"
                    }
                }
            )
            .then(data => {
                return true;
            })
            .catch(err => {
                return false;
            });
    }
    //Socket para Asignamiento de conversaciones a los Agentes
    socket.on("Agent", async(room, payload) => {
        //cada vez que el usuario se conecta o que reinicia la página

        if (payload.idSocketAgent) {
            socket.join(room);
            var result = agentOnline.find(
                soc => soc.idAgent === payload.idSocketAgent
            );
            if (result === undefined) {
                agentOnline.push({
                    idAgent: payload.idSocketAgent,
                    numCon: payload.numeroCon,
                    sockId: socket.id,
                    state: payload.stateAgent,
                    connect: true,
                    nameAgent: payload.nameAgent,
                    capacity: payload.capacity,
                    idAg: payload.id
                });
                let pos = searchIndexAgent(payload.idSocketAgent);


                if (
                    payload.stateAgent === "ONLINE" ||
                    payload.stateAgent === "OCUPADO"
                ) {
                    addEventimerOnline(timerOnline, socket, pos);
                    addEvenTimeBreak(timerBreak, socket, pos);
                    timerOnline.start();
                } else if (payload.stateAgent === "BREAK") {
                    addEventimerOnline(timerOnline, socket, pos);
                    addEvenTimeBreak(timerBreak, socket, pos);
                    timerBreak.start();
                }
            } else if (result.state === "ONLINE") {
                try {
                    let pos = searchIndexAgent(payload.idSocketAgent);
                    agentOnline[pos].sockId = socket.id;
                    agentOnline[pos].connect = true;
                    addEventimerOnline(timerOnline, socket, pos);
                    addEvenTimeBreak(timerBreak, socket, pos);
                    var valueinSeconds = convertToInt(agentOnline[pos].timerOnline);
                    precisionOnline(timerOnline, valueinSeconds);
                    socket.emit("timer_break", agentOnline[pos].timerBreak);
                    checkChatsCola(room, pos);
                } catch (e) {
                    logger.error(
                        "Error, no existe propiedad timerOnline en array de objetos agentOnline: " +
                        e
                    );
                }
            } else if (result.state === "OCUPADO") {
                try {
                    let pos = searchIndexAgent(payload.idSocketAgent);
                    agentOnline[pos].sockId = socket.id;
                    agentOnline[pos].connect = true;
                    addEventimerOnline(timerOnline, socket, pos);
                    addEvenTimeBreak(timerBreak, socket, pos);
                    var valueinSeconds = convertToInt(agentOnline[pos].timerOnline);
                    precisionOnline(timerOnline, valueinSeconds);
                    socket.emit("timer_break", agentOnline[pos].timerBreak);
                } catch (e) {
                    logger.error(
                        "Error, no existe propiedad timerOnline en array de objetos agentOnline: " +
                        e
                    );
                }
            } else if (result.state === "BREAK") {
                try {
                    let pos = searchIndexAgent(payload.idSocketAgent);
                    agentOnline[pos].sockId = socket.id;
                    agentOnline[pos].connect = true;
                    addEventimerOnline(timerOnline, socket, pos);
                    addEvenTimeBreak(timerBreak, socket, pos);
                    var valueinSeconds = convertToInt(agentOnline[pos].timerBreak);
                    precisionBreak(timerBreak, valueinSeconds);
                    socket.emit("timer_online", agentOnline[pos].timerOnline);
                } catch (e) {
                    logger.error(
                        "Error, no existe propiedad timerBreak en array de objetos agentOnline: " +
                        e
                    );
                }
            }
            //Cada vez que el usuario cambia a estado ONLINE
        } else if (payload.checkCola) {
            socket.join(room);
            let pos = searchIndexAgent(room);
            try {
                if (payload.newStat) {
                    if (agentOnline[pos].timerOnline) {
                        var valueinSeconds = convertToInt(agentOnline[pos].timerOnline);
                        precisionOnline(timerOnline, valueinSeconds);
                    } else {
                        addEventimerOnline(timerOnline, socket, pos);
                        timerOnline.start();
                    }
                    timerBreak.stop();
                    agentOnline[pos].state = payload.newStat;
                }
                checkChatsCola(room, pos);
            } catch (e) {
                logger.error(
                    "Error, no existe propiedad State en array de objetos agentOnline: " +
                    e
                );
            }
            //Cada vez que el usuario cambia de estado
        } else if (payload.newState) {
            let pos = searchIndexAgent(room);
            if (payload.newState != "OFFLINE") {
                try {
                    agentOnline[pos].connect = true;
                    agentOnline[pos].state = payload.newState;
                    if (payload.newState != "ONLINE") {
                        if (payload.newState == "OCUPADO") {
                            if (agentOnline[pos].timerOnline) {
                                var valueinSeconds = convertToInt(agentOnline[pos].timerOnline);
                                timerBreak.stop();
                                precisionOnline(timerOnline, valueinSeconds);
                            } else {
                                addEventimerOnline(timerOnline, socket, pos);
                                timerBreak.stop();
                                timerOnline.start();
                            }
                        } else if (agentOnline[pos].timerBreak) {
                            var valueinSeconds = convertToInt(agentOnline[pos].timerBreak);
                            timerOnline.stop();
                            precisionBreak(timerBreak, valueinSeconds);
                        } else {
                            addEvenTimeBreak(timerBreak, socket, pos);
                            timerOnline.stop();
                            timerBreak.start();
                        }
                    }
                } catch (e) {
                    logger.error(
                        "Error, no existe propiedad del array de objetos agentOnline: " + e
                    );
                }
            } else {
                agentOnline.splice(pos, 1);
                timerOnline.stop();
                timerBreak.stop();
            }
            //Cada vez que ingresa un nuevo usuario
        } else if (payload.id) {
            if (timeStart) {
                asignAgent(socket, payload);
            } else {
                const data = await isHabilDay();
                timeStart = data.hourstart;
                timeStop = data.hourend;
                logger.info(
                    "Horario de atención del día de hoy inicia;" +
                    timeStart +
                    " finalzia: " +
                    timeStop
                );
                asignAgent(socket, payload);
            }
        }
        var result = agentOnline.filter(soc => {
            return (
                soc.state === "ONLINE" &&
                soc.connect === true &&
                soc.numCon < soc.capacity
            );
        });
        io.sockets.emit("getAgentsOnline", result);
    });
    socket.on("getAgentsOnline", () => {
        var result = agentOnline.filter(soc => {
            return (
                soc.state === "ONLINE" &&
                soc.connect === true &&
                soc.numCon < soc.capacity
            );
        });
        io.sockets.emit("getAgentsOnline", result);
    });
    //Canal privado establecido entre Agente y Usuario
    socket.on("Agent-User", async function(
        room,
        msg,
        stc,
        idcon,
        channel,
        who,
        pathFile
    ) {
        //suscripción al canal
        socket.join(room);

        /* Reinicar bot cuando esta esperando agente */
        if (msg && msg.toLowerCase().includes("hablar con Laika bot")) {
            let pos = searchIndexChatPen(room);
            if (pos !== -1) {
                socket.emit("new-message", {
                    msg: "Hola nuevamente, escribe menú para continuar",
                    action: "resetChat"
                });
            }
            chatsPendientes.splice(pos, 1);
        }
        /*TMO*/

        if (who === "tmoReset") {
            timeChat.reset();
            timeChatFin.stop();
        } else if (who === "tmo") {
            var resultTmo = tmo.find(soc => soc.id === room);
            if (resultTmo === undefined) {
                tmo.push({ id: room });
            }
            try {
                timeChat.start({ countdown: true, startValues: { seconds: warning } });
                let pos = searchIndexTmo(room);
                addEventimeChat(
                    timeChat,
                    timeChatFin,
                    socket,
                    pos,
                    room,
                    channel,
                    idcon
                );
            } catch (e) {
                logger.error("Error: " + e);
            }
        } else if (who == "tmoFin") {
            timeChat.stop();
            timeChatFin.stop();
            timeChat = new Timer();
            timeChatFin = new Timer();
        }

        if (who === "user" && msg == null) {
            var resultTmo = tmo.find(soc => soc.id === room);
            if (resultTmo !== undefined) {
                let pos = searchIndexTmo(room);
                var valueinSeconds = convertToInt(tmo[pos].time);
                precisionTmo(timeChat, valueinSeconds);
                addEventimeChat(
                    timeChat,
                    timeChatFin,
                    socket,
                    pos,
                    room,
                    channel,
                    idcon
                );
            }
        }
        /*TMO*/

        if (stc == null || stc == 2) {
            if (msg != null) {
                allMsg += 1;
            }
            if (channel === "webchat") {
                if (!pathFile) {
                    socket.broadcast.to(room).emit("private-message", {
                        socketIdUser: room,
                        messageUser: msg,
                        state: stc,
                        idCon: idcon,
                        who: who
                    });
                } else {
                    if (msg) {
                        let tempMsg = msg.split(",")[1];
                        const imageBuffer = Buffer.from(tempMsg, "base64");
                        fs.writeFileSync(
                            `src/public/sended-files/${pathFile}`,
                            imageBuffer
                        );
                        socket.broadcast.to(room).emit("private-message", {
                            socketIdUser: room,
                            messageUser: `https://chatbots.montechelo.com.co/widget/laika/sended-files/${pathFile}`,
                            state: stc,
                            idCon: idcon,
                            who: who
                        });
                    }
                }
            } else if (channel === "Facebook Messenger" && stc === 2) {
                let pos = usersOfMessenger.findIndex(ele => ele.idSocket === room);
                usersOfMessenger[pos].idCon = idcon;
                /*TMO*/
                var resultTmo = tmo.find(soc => soc.id === room);
                if (resultTmo === undefined) {
                    tmo.push({ id: usersOfMessenger[pos].idSocket });
                }
                try {
                    usersOfMessenger[pos].timeChat.start({
                        countdown: true,
                        startValues: { seconds: warning }
                    });
                    let posTmo = searchIndexTmo(usersOfMessenger[pos].idSocket);
                    addEventimeChat(
                        usersOfMessenger[pos].timeChat,
                        usersOfMessenger[pos].timeChatFin,
                        null,
                        posTmo,
                        usersOfMessenger[pos].idSocket,
                        "Facebook Messenger",
                        usersOfMessenger[pos].idCon
                    );
                } catch (e) {
                    logger.error("Error: " + e);
                }
                /*TMO*/
                if (msg) {
                    try {
                        await tasksFunctions.saveMessageAgent(
                            usersOfMessenger[pos].jwt,
                            usersOfMessenger[pos].id,
                            usersOfMessenger[pos].idCon,
                            msg,
                            "agent"
                        )
                        await facebookMessengerIntegration.methods.sendMessage(room, {
                            "text": msg
                        });
                    } catch (error) {
                        console.error(error);
                    }
                }
            } else if (channel === "Facebook Messenger" && stc === null) {
                if (!pathFile) {
                    if (msg) {
                        let pos = usersOfMessenger.findIndex(ele => ele.idSocket === room);
                        socket.broadcast.to(room).emit("private-message", {
                            socketIdUser: room,
                            messageUser: msg,
                            state: stc,
                            idCon: idcon,
                            who: who
                        });
                        usersOfMessenger[pos].timeChat.reset();
                        usersOfMessenger[pos].timeChatFin.stop();
                        await facebookMessengerIntegration.methods.sendMessage(room, {
                            "text": msg
                        });
                    }
                } else {
                    if (msg) {
                        let tempMsg = msg.split(",")[1];
                        const imageBuffer = Buffer.from(tempMsg, "base64");
                        fs.writeFileSync(
                            `src/public/sended-files/${pathFile}`,
                            imageBuffer
                        );
                        let pos = usersOfMessenger.findIndex(ele => ele.idSocket === room);
                        socket.broadcast.to(room).emit("private-message", {
                            socketIdUser: room,
                            messageUser: `https://chatbots.montechelo.com.co/widget/laika/sended-files/${pathFile}`,
                            state: stc,
                            idCon: idcon,
                            who: who
                        });
                        usersOfMessenger[pos].timeChat.reset();
                        usersOfMessenger[pos].timeChatFin.stop();

                        let attachmentMessage = "";

                        if (/\.(jpe?g|png|gif|bmp)/i.test(pathFile)) {
                            attachmentMessage = {
                                "attachment": {
                                    "type": "image",
                                    "payload": {
                                        "url": `https://chatbots.montechelo.com.co/widget/laika/sended-files/${pathFile}`,
                                        "is_reusable": true
                                    }
                                }
                            }
                        } else if (/\.(undefined|pdf)/i.test(pathFile)) {
                            attachmentMessage = {
                                "attachment": {
                                    "type": "file",
                                    "payload": {
                                        "url": `https://chatbots.montechelo.com.co/widget/laika/sended-files/${pathFile}`,
                                        "is_reusable": true
                                    }
                                }
                            }
                        } else {
                            logger.error('Formato de archivo no contemplado ' + pathFile);
                        }
                        await facebookMessengerIntegration.methods.sendMessage(room, attachmentMessage);
                    }
                }
            } else if (channel === "whatsapp" && stc === 2) {
                let pos = searchIndexWhatsapp(room);
                userWhatsapp[pos].idCon = idcon;
                /*TMO*/
                var resultTmo = tmo.find(soc => soc.id === room);
                if (resultTmo === undefined) {
                    tmo.push({ id: userWhatsapp[pos].idSocket });
                }
                try {
                    userWhatsapp[pos].timeChat.start({
                        countdown: true,
                        startValues: { seconds: warning }
                    });
                    let posTmo = searchIndexTmo(userWhatsapp[pos].idSocket);
                    addEventimeChat(
                        userWhatsapp[pos].timeChat,
                        userWhatsapp[pos].timeChatFin,
                        null,
                        posTmo,
                        userWhatsapp[pos].idSocket,
                        "whatsapp",
                        userWhatsapp[pos].idCon
                    );
                } catch (e) {
                    logger.error("Error: " + e);
                }
                /*TMO*/
                if (msg) {
                    try {
                        await tasksFunctions.saveMessageAgent(
                            userWhatsapp[pos].jwt,
                            userWhatsapp[pos].id,
                            userWhatsapp[pos].idCon,
                            msg,
                            "agent"
                        )
                        utilMessages.sendMessage(room, msg);
                    } catch (error) {
                        console.error(error);
                    }
                }
            } else if (channel === "whatsapp" && stc === null) {
                if (!pathFile) {
                    if (msg) {
                        let pos = searchIndexWhatsapp(room);
                        socket.broadcast.to(room).emit("private-message", {
                            socketIdUser: room,
                            messageUser: msg,
                            state: stc,
                            idCon: idcon,
                            who: who
                        });
                        userWhatsapp[pos].timeChat.reset();
                        userWhatsapp[pos].timeChatFin.stop();
                        utilMessages.sendMessage(room, msg);
                    }
                } else {
                    if (msg) {


                        let tempMsg = msg.split(",")[1];
                        const imageBuffer = Buffer.from(tempMsg, "base64");
                        fs.writeFileSync(
                            `src/public/sended-files/${pathFile}`,
                            imageBuffer
                        );
                        let imgaeWha = `https://chatbots.montechelo.com.co/widget/laika/sended-files/${pathFile}`;
                        let res = imgaeWha.split(".");
                        var longitud = res.length;
                        var typeFile = res[longitud - 1].toLowerCase();

                        let pos = searchIndexWhatsapp(room);
                        // socket.broadcast.to(room).emit("private-message", {
                        //     socketIdUser: room,
                        //     messageUser: `https://chatbots.montechelo.com.co/widget/Hughes/sended-files/${pathFile}`,
                        //     state: stc
                        //     idCon: idcon,
                        //     who: who
                        // });
                        userWhatsapp[pos].timeChat.reset();
                        userWhatsapp[pos].timeChatFin.stop();
                        if (typeFile == "png" || typeFile == "jpg" || typeFile == "jpeg" || typeFile == "svg") {
                            console.log("ENVIANDO IMAGENES")
                            sendImaggeWha(room, imgaeWha);
                        } else if (typeFile == "pdf" || typeFile == "xlsx") {
                            console.log("ENVIANDO ARCHIVOS")
                            sendFyleWha(room, imgaeWha);
                        } else {
                            console.log("ARCHIVO NO VALIDO")
                        }



                    }
                }
            }
        } else if (stc == 1) {
            let templates = await tasksFunctions.templates();
            let plant = [];
            templates.forEach((e) => {
                if (e.id === 2) {
                    plant.push(e)
                }
            })
            chatsFinalizados += 1;
            if (channel === "webchat") {
                socket.broadcast.to(room).emit("private-message", {
                    socketIdUser: room,
                    messageUser: plant[0].valor,
                    state: stc,
                    idCon: idcon
                });
            } else if (channel === "Facebook Messenger") {
                let pos = usersOfMessenger.findIndex(ele => ele.idSocket === room);
                usersOfMessenger[pos].bot = false;
                usersOfMessenger[pos].timeChat.stop();
                usersOfMessenger[pos].timeChatFin.stop();
                usersOfMessenger[pos].timeChat = new Timer();
                usersOfMessenger[pos].timeChatFin = new Timer();
                const textMessage = plant[0].valor;
                await facebookMessengerIntegration.methods.sendMessage(room, {
                    "text": textMessage
                });
            } else if (channel === "whatsapp") {
                let pos = searchIndexWhatsapp(room);
                userWhatsapp[pos].bot = false;
                userWhatsapp[pos].timeChat.stop();
                userWhatsapp[pos].timeChatFin.stop();
                userWhatsapp[pos].timeChat = new Timer();
                userWhatsapp[pos].timeChatFin = new Timer();
                utilMessages.sendMessage(room, plant[0].valor);
            }
            let pos = searchIndexAgent(msg);
            try {
                if (agentOnline[pos].numCon > 0) {
                    agentOnline[pos].numCon -= 1;
                }
            } catch {
                logger.error(
                    "Error, no existe propiedad numCon en array de objetos agentOnline"
                );
            }
        }
    });
    //Nuevo horario
    socket.on("new_office_hour", async(data) => {
        switch (data) {
            case 1:
                const resp = await isHabilDay();
                timeStart = resp.hourstart;
                timeStop = resp.hourend;
                logger.info(
                    "Nuevo Horario de atención del día de hoy inicia;" +
                    timeStart +
                    " finalzia: " +
                    timeStop
                );
                break;
        }
    });
    //Nueva Capacidad Agente
    socket.on("new_capacity", function(data) {
        try {
            let pos = searchIndexAgent(data.agent);
            if (pos != -1) {
                agentOnline[pos].capacity = data.newCap;
                io.sockets.emit("new_info", {
                    agent: data.agent,
                    capacity: data.newCap
                });
            }
        } catch (e) {
            logger.error(
                "Error, no existe propiedad numCon en array de objetos agentOnline" + e
            );
        }
    });
    //Nuevo TMO
    socket.on("new_tmo", function(data) {
        try {
            warning = data.warning * 60;
            ending = data.ending * 60;
        } catch (e) {
            logger.error(
                "Error, no existe propiedad numCon en array de objetos agentOnline" + e
            );
        }
    });
    //Nuevo QR
    socket.on("new_qr", data => {
        if (data === 1) {
            runSulla();
        }
    });
    //Mensajes entre Dialogflow y Usuario
    socket.on("new-message", async function(data) {
        var sessionId = "{0.id}".format(data);
        var message = data.message;
        /* const obj = await detectTextIntent(
          projectId,
          sessionId,
          message,
          languageCode
        ); */
        try {
            /* var actionQuery = obj[0].queryResult.action; */
            var actionQuery = "menuc";

            /*Para sumar al count de widget polymer
            if(actionQuery == 'default'){
              socket.emit('estado', 'nuevo');
            }*/
            if (
                actionQuery == "menuc" ||
                actionQuery == "menud"
            ) {
                socket.emit("estado", "agente");
            }
            var mensajes = obj[0].queryResult.fulfillmentMessages;
            mensajes.forEach(function(e) {
                if (e.platform === "ACTIONS_ON_GOOGLE") {
                    try {
                        var textToSpeech =
                            e.simpleResponses.simpleResponses[0].textToSpeech;
                        socket.emit("new-message", {
                            msg: textToSpeech,
                            action: actionQuery
                        });
                    } catch {
                        if (obj[0].queryResult.action == "input.welcome") {
                            var servicios = [
                                { title: "Medicina Prepagada" },
                                { title: "Servicio veterinario" },
                                { title: "Solicitudes, quejas y reclamos" },
                                { title: "Hacer un pedido" },
                                { title: "Pedidos en curso" },
                                { title: "Precio de productos" },
                                { title: "Quiero 15% de descuento permanente" }
                            ];
                            socket.emit("new-message", {
                                sug: servicios,
                                action: actionQuery
                            });
                        } else {
                            var suggestions = e.suggestions.suggestions;
                            socket.emit("new-message", {
                                sug: suggestions,
                                action: actionQuery
                            });
                        }
                    }
                } else if (e.platform === "KIK") {
                    var textToSpeech = e.text.text[0];
                    socket.emit("new-message", {
                        msg: textToSpeech,
                        action: actionQuery
                    });
                }
            });
        } catch {
            logger.error("Error, no hay resultado del API de Dialogflow");
        }
    });
});
/*
  Determina el puerto por el cual se va desplegar la aplicación
*/
server.listen(5000, function() {
    logger.info("Server On en https://chatbots.montechelo.com.co/widget/laika");
});
/*
 Función para detectar intentos de Dialogflow y retornar queryResult
*/
function detectTextIntent(projectId, sessionId, queries, languageCode) {
    // [START dialogflow_detect_intent_text]
    // Imports the Dialogflow library
    const dialogflow = require("dialogflow");
    // Instantiates a session client
    const sessionClient = new dialogflow.SessionsClient();
    // The path to identify the agent that owns the created intent.
    const sessionPath = sessionClient.sessionPath(projectId, sessionId);
    //variable for save object response from Dialogflow
    let response;
    // The text query request.
    const request = {
        session: sessionPath,
        queryInput: {
            text: {
                text: queries,
                languageCode: languageCode
            }
        }
    };
    //detect Intent for request
    response = sessionClient.detectIntent(request);
    //return object with queryResult
    return response;
}
/*
 Función para buscar indice en array de agentes segun valor del Agent ID
*/
function searchIndexAgent(value) {
    pos = agentOnline.findIndex(ele => ele.idAgent === value);
    return pos;
}
/*
 Función para buscar indice en array de agentes segun valor del Socket ID
*/
function searchIndexAgentSocket(value) {
    pos = agentOnline.findIndex(ele => ele.sockId === value);
    return pos;
}
/*
Función para buscar indice en array de chats pendientes segun el socketid
+*/
function searchIndexChatPen(value) {
    pos = chatsPendientes.findIndex(ele => ele.socketId === value);
    return pos;
}
/*
 Función para buscar indice en array de supervisor segun valor del Agent ID
*/
function searchIndexSuperv(value) {
    pos = supervisorOnline.findIndex(ele => ele.idAgent === value);
    return pos;
}

/*
 Función para buscar indice en array de agentes segun valor del Agent ID
*/
function searchIndexTmo(value) {
    pos = tmo.findIndex(ele => ele.id === value);
    return pos;
}
/*
 Función para buscar indice en array de supervisor segun valor del Socket ID
*/
function searchIndexAgentSuperv(value) {
    pos = supervisorOnline.findIndex(ele => ele.sockId === value);
    return pos;
}
/*
 Función para buscar indice en array de agentes, con menos conversaciones
*/
function findMinCon(arr) {
    let min = arr[0].numCon,
        ind = 0,
        v;
    for (let i = 1, len = arr.length; i < len; i++) {
        v = arr[i].numCon;
        v < min ? ((min = v), (ind = i)) : ((min = min), ind);
    }
    return ind;
}
/*
  Funcion para convertir formato de hora-minutos-segundos hh:mm:ss a segundos
*/
function convertToInt(num) {
    try {
        let arr = num.split(":");
        let hr = parseInt(arr[0]);
        let min = parseInt(arr[1]);
        let seg = parseInt(arr[2]);
        let result = hr * 3600 + min * 60 + seg;
        return result;
    } catch {
        logger.error("Error, no existe array para hacerle split");
    }
}
/*
  Funcion para convertir segundos a formato de hora-minutos hh:mm am/pm
*/
function convertToHour(num) {
    try {
        var hours = Math.floor(num / 3600);
        var minutes = Math.floor((num % 3600) / 60);
        if (hours < 10) hours = "0" + hours;
        if (minutes < 10) minutes = "0" + minutes;
        if (hours > 12) {
            hours = hours - 12;
            apm = "pm";
        } else if (hours == 12) {
            apm = "pm";
        } else {
            apm = "am";
        }
        return hours + ":" + minutes + " " + apm;
    } catch {
        logger.error("Error, no existe array para hacerle split");
    }
}
/*
  Funcion para obtener formato de año-mes-dia yyyy:mm:dd
*/
function nowDate() {
    var date = new Date();
    var aaaa = date.getFullYear();
    var gg = date.getDate();
    var mm = date.getMonth() + 1;
    if (gg < 10) gg = "0" + gg;
    if (mm < 10) mm = "0" + mm;
    var cur_day = aaaa + "-" + mm + "-" + gg;
    return cur_day;
}
/*
  Funcion para obtener formato de hora-minutos-segundos hh:mm:ss
*/
function nowHour() {
    var date = new Date();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    if (hours < 10) hours = "0" + hours;
    if (minutes < 10) minutes = "0" + minutes;
    if (seconds < 10) seconds = "0" + seconds;
    return hours + ":" + minutes + ":" + seconds;
}
/*
  Funcion para obtener formato de hora-minutos-segundos hh_mm_ss
*/
function nowHourTag() {
    var date = new Date();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    if (hours < 10) hours = "0" + hours;
    if (minutes < 10) minutes = "0" + minutes;
    if (seconds < 10) seconds = "0" + seconds;
    return hours + "_" + minutes + "_" + seconds;
}
/*
  Funcion para llamada de horario de atención del día
*/
async function isHabilDay() {
    try {
        const data = await tasksFunctions.isDayHabil(nowDate());
        return data[0];
    } catch (error) {
        logger.error(
            "Error, no existe propiedad numCon en array de objetos agentOnline: " +
            err
        );
        return null;
    }
}
/*
  Funcion para llamada de horario de atención del día
*/
async function tmoActual() {
    return await tasksFunctions.tmo((err, data) => {
        if (!err) {
            warning = data[0].warning * 60;
            ending = data[0].ending * 60;
        }
    });
}
/*
  Funcion para asignamiento de agentes, según horario de atención
*/
async function asignAgent(socket, payload) {
    let templates = await tasksFunctions.templates();
    let plant = [];
    templates.forEach((e) => {
        if (e.id === 3) {
            plant.push(e)
        }
    })
    var hour = convertToInt(nowHour());
    if (hour >= timeStart && hour <= timeStop) {
        var result = agentOnline.filter(
            soc =>
            soc.state === "ONLINE" &&
            soc.connect === true &&
            soc.numCon < soc.capacity
        );
        if (result === undefined || result.length === 0) {
            socket.emit("new-message", {
                msg: plant[0].valor,
                action: null
            });
            chatsPendientes.push(payload);
            socket.emit("messages", null);
        } else {
            try {
                let x = findMinCon(result);
                socket.broadcast
                    .to(result[x].idAgent)
                    .emit("event", payload, "webchat");
                let pos = searchIndexAgent(result[x].idAgent);
                agentOnline[pos].numCon += 1;
            } catch (e) {
                logger.error("Error asignando conversacion al agente: " + e);
            }
        }
    } else {
        socket.emit("new-message", {
            msg: plant[0].valor,
            action: "resetChat"
        });
    }
}
/*
  Función para asignar Chat en Cola al agente que finaliza conversación
*/
function checkChatsCola(room, pos) {
    let capacity = agentOnline[pos].capacity;
    let conversation = agentOnline[pos].numCon;
    let capTot = capacity - conversation;
    if (chatsPendientes.length > 0 && conversation < capacity) {
        chatsPendientes.slice(0, capTot).forEach(e => {
            io.in(room).emit("event", e, e.channel);
            try {
                agentOnline[pos].numCon += 1;
            } catch (e) {
                logger.error(
                    "Error, no existe propiedad numCon en array de objetos agentOnline: " +
                    e
                );
            }
        });
        chatsPendientes.splice(0, capTot);
    }
}
/*
  Función para asignar Listener a la instancia de easytimer 
*/
function addEventimerOnline(timerOnline, socket, pos) {
    try {
        timerOnline.addEventListener("secondsUpdated", function(e) {
            agentOnline[pos].timerOnline = timerOnline.getTimeValues().toString();
            socket.emit("timer_online", timerOnline.getTimeValues().toString());
        });
    } catch (e) {
        logger.error("Error añaniendo evento timer: " + e);
    }
}
/*
  Función para asignar Listener a la instancia de easytimer 
*/
function addEvenTimeBreak(timerBreak, socket, pos) {
    try {
        timerBreak.addEventListener("secondsUpdated", function(e) {
            agentOnline[pos].timerBreak = timerBreak.getTimeValues().toString();
            socket.emit("timer_break", timerBreak.getTimeValues().toString());
        });
    } catch (e) {
        logger.error("Error añaniendo evento timer: " + e);
    }
}
/*
  Función para asignar Listener a la instancia de easytimer 
*/
function addEventimerOnlineSup(timerOnline, socket, pos) {
    try {
        timerOnline.addEventListener("secondsUpdated", function(e) {
            supervisorOnline[
                pos
            ].timerOnline = timerOnline.getTimeValues().toString();
            socket.emit("timer_online", timerOnline.getTimeValues().toString());
        });
    } catch (e) {
        logger.error("Error añaniendo evento timer: " + e);
    }
}

/*
  Función para asignar Listener a la instancia de easytimer 
*/
function addEvenTimeBreakSup(timerBreak, socket, pos) {
    try {
        timerBreak.addEventListener("secondsUpdated", function(e) {
            supervisorOnline[pos].timerBreak = timerBreak.getTimeValues().toString();
            socket.emit("timer_break", timerBreak.getTimeValues().toString());
        });
    } catch (e) {
        logger.error("Error añaniendo evento timer: " + e);
    }
}

/*
  Función para iniciar el contador de tiempo Online en los segundos precisos que estaba
*/
function precisionOnline(timerOnline, valueinSeconds) {
    try {
        timerOnline.start({
            precision: "seconds",
            startValues: { seconds: valueinSeconds }
        });
    } catch (e) {
        logger.error("Error iniciando timer: " + e);
    }
}
/*
  Función para iniciar el contador de tiempo en Break los segundos precisos que estaba
*/
function precisionBreak(timerBreak, valueinSeconds) {
    try {
        timerBreak.start({
            precision: "seconds",
            startValues: { seconds: valueinSeconds }
        });
    } catch (e) {
        logger.error("Error iniciando timer: " + e);
    }
}
/*
  Función para asignar Listener a la instancia de easytimer 
*/
function addEventimeChat(timeC, timeCFin, socket, pos, room, channel, idCon) {
    try {
        tmo[pos].time = timeC.getTimeValues().toString();
        timeC.addEventListener("secondsUpdated", function(e) {
            tmo[pos].time = timeC.getTimeValues().toString();
        });
        timeC.addEventListener("reset", function(e) {
            tmo[pos].time = timeC.getTimeValues().toString();
        });
        timeC.addEventListener("targetAchieved", async function(e) {
            io.in(room).emit("private-message", {
                socketIdUser: room,
                messageUser: "Tu conversación se finalizará en " +
                    ending / 60 +
                    " minuto(s) si no respondes algo",
                state: 4,
                idCon: idCon,
                who: "agent"
            });
            timeC.stop();
            if (channel === "webchat") {
                timeCFin.start({ countdown: true, startValues: { seconds: ending } });
                let pos = searchIndexTmo(room);
                addEventimeChatFin(timeCFin, socket, pos, room, channel, idCon);
            } else if (channel === "Facebook Messenger") {
                const textMessage = `Tu conversación se finalizará en ${ending / 60} minuto(s) si no respondes algo.`
                await facebookMessengerIntegration.methods.sendMessage(room, {
                    "text": textMessage
                });
                timeCFin.start({ countdown: true, startValues: { seconds: ending } });
                let pos = searchIndexTmo(room);
                addEventimeChatFin(timeCFin, socket, pos, room, channel, idCon);
            } else if (channel === "whatsapp") {
                utilMessages.sendMessage(room, "Tu conversación se finalizará en " +
                    ending / 60 +
                    " minuto(s) si no respondes algo");
                timeCFin.start({ countdown: true, startValues: { seconds: ending } });
                let pos = searchIndexTmo(room);
                addEventimeChatFin(timeCFin, socket, pos, room, channel, idCon);
            }
        });
    } catch (e) {
        logger.error("Error añaniendo evento timer: " + e);
    }
}
/*
  Función para asignar Listener a la instancia de easytimer 
*/
function addEventimeChatFin(timeCF, socket, pos, room, channel, idCon) {
    try {
        tmo[pos].timeFin = timeCF.getTimeValues().toString();
        timeCF.addEventListener("secondsUpdated", function(e) {
            tmo[pos].timeFin = timeCF.getTimeValues().toString();
        });
        timeCF.addEventListener("reset", function(e) {
            tmo[pos].timeFin = timeCF.getTimeValues().toString();
        });
        timeCF.addEventListener("targetAchieved", function(e) {
            if (channel === "webchat") {
                io.in(room).emit("private-message", {
                    socketIdUser: room,
                    messageUser: null,
                    state: 5,
                    idCon: idCon,
                    who: "agent",
                    channel: channel
                });
            } else if (channel === "Facebook Messenger") {
                io.in(room).emit("private-message", {
                    socketIdUser: room,
                    messageUser: null,
                    state: 5,
                    idCon: idCon,
                    who: "agent",
                    channel: channel
                });
                let pos = usersOfMessenger.findIndex(ele => ele.idSocket === room);
                usersOfMessenger[pos].bot = false;
                usersOfMessenger[pos].timeChat.stop();
                usersOfMessenger[pos].timeChatFin.stop();
                usersOfMessenger[pos].timeChat = new Timer();
                usersOfMessenger[pos].timeChatFin = new Timer();
            } else if (channel === "whatsapp") {
                io.in(room).emit("private-message", {
                    socketIdUser: room,
                    messageUser: null,
                    state: 5,
                    idCon: idCon,
                    who: "agent",
                    channel: channel
                });
                let posWha = searchIndexWhatsapp(room);
                userWhatsapp[posWha].bot = false;
                userWhatsapp[posWha].timeChat.stop();
                userWhatsapp[posWha].timeChatFin.stop();
                userWhatsapp[posWha].timeChat = new Timer();
                userWhatsapp[posWha].timeChatFin = new Timer();
            }
        });
    } catch (e) {
        logger.error("Error añaniendo evento timer: " + e);
    }
}
/*
  Función para iniciar el contador de tiempo en chat los segundos precisos que estaba
*/
function precisionTmo(timeC, valueinSeconds) {
    try {
        timeC.start({
            countdown: true,
            startValues: { seconds: valueinSeconds }
        });
    } catch (e) {
        logger.error("Error iniciando timer: " + e);
    }
}


/***********************************************************************************************************
 *********************************************** WHATSAPP OFICIAL ******************************************
 ***********************************************************************************************************/
//Endpoint para recibir mensajes de WhatsApp

app.post("/widget/laika/tasks/messages", async(req, res) => {
    let receivedMessages = req.body.results;
    let messageBody = req.body.results[0];
    let { message: { type } } = messageBody;
    var fromWha = req.body.results[0].from;
    var msgWha = req.body.results[0].message.text;

    if (type == 'TEXT') {

        var bodyMessage = req.body.results[0].message;
        enviar(fromWha, msgWha, receivedMessages);
        res.send("Entry point working for text");

    } else if (type == 'VOICE') {

        let now = nowHourTag()
        let path = fromWha + now;
        let { message: { url } } = messageBody;
        let audio = await guardarAudio(url, path);
        if (audio) {
            let msg = `https://chatbots.montechelo.com.co/widget/laika/sended-files/${path}.oga`;
            enviarAudio(fromWha, msg, receivedMessages);
        }
        res.send("Entry point working for audio");

    } else if (type == 'DOCUMENT') {
        let { message: { url } } = messageBody;
        let { message: { caption } } = messageBody;

        let exten = (/[.]/.exec(caption)) ? /[^.]+$/.exec(caption)[0] : undefined;

        if (exten) {
            let document = await guardarDocumento(url, caption);

            if (document) {
                let msg = `https://chatbots.montechelo.com.co/widget/laika/sended-files/${caption}`;
                enviar(fromWha, msg, receivedMessages)
            }
        }

        res.send("Entry point working for document");
    } else if (type == 'VIDEO') {

        let now = nowHourTag()
        let path = fromWha + now;
        let { message: { url } } = messageBody;

        let video = await guardarVideo(url, path);

        if (video) {
            let msg = `https://chatbots.montechelo.com.co/widget/laika/sended-files/${path}.mp4`;
            enviar(fromWha, msg, receivedMessages);
        }
        res.send("Entry point working for video");

    } else if (type == 'IMAGE') {

        let now = nowHourTag()
        let path = fromWha + now;
        let { message: { url } } = messageBody;

        let image = await guardarImagen(url, path);

        if (image) {
            let msg = `https://chatbots.montechelo.com.co/widget/laika/sended-files/${path}.jpeg`;
            enviar(fromWha, msg, receivedMessages)
        }
        res.send("Entry point working for image");
    }
});


async function enviar(from, message, receivedMessages) {
    logger.info('Enviando mensajes de WhatsApp')

    let idUserWa = from;
    let nameWa = from;
    let messWa = message;
    let result = userWhatsapp.find(soc => soc.idSocket === idUserWa);
    let a = await checkandSave(result, idUserWa, nameWa);
    result = userWhatsapp.find(soc => soc.idSocket === idUserWa);
    let pos = searchIndexWhatsapp(idUserWa);

    if (result.bot === false) {
        DialogFlow(pos, idUserWa, messWa, receivedMessages);
    } else {
        BotHuman(pos, idUserWa, messWa);
    }

};

async function enviarAudio(from, message, receivedMessages) {
    logger.info('Enviando archivos en WhatsApp')

    let idUserWa = from;
    let nameWa = from;
    let messWa = message;
    let result = userWhatsapp.find(soc => soc.idSocket === idUserWa);
    let a = await checkandSave(result, idUserWa, nameWa);
    result = userWhatsapp.find(soc => soc.idSocket === idUserWa);
    let pos = searchIndexWhatsapp(idUserWa);

    if (result.bot === false) {
        DialogFlow(pos, idUserWa, messWa, receivedMessages);
    } else {
        BotHumanAudio(pos, idUserWa, messWa);
    }

};

//Envia mensajes del bot (DialogFlow)
async function DialogFlow(pos, idUserWa, messWa, receivedMessages) {
    receivedMessages.forEach(async receivedMsg => {
        var messagesToSend = await utilMessages.processMessage(
            receivedMsg.from,
            receivedMsg.message.text
        );
        messagesToSend.forEach(msg => {
            utilMessages.sendMessage(
                receivedMsg.from,
                msg
            ); // Envia mensaje a Whatsapp
        });
    });
    try {
        await tasksFunctions.saveMessage(
            userWhatsapp[pos].jwt,
            userWhatsapp[pos].id,
            messWa,
            "user"
        )
        var sessionId = idUserWa.format(messWa);
        var messages = messWa;
        const obj = await detectTextIntent(projectId, sessionId, messages, languageCode);
        var actionQuery = obj[0].queryResult.action;
    } catch (error) {
        console.error(error);
    }
    /*
        Asignación de agente desde whatsapp
    */
    if (actionQuery == 'human') {
        if (timeStart) {
            userWhatsapp[pos].bot = true;
            asignAgentWhat(idUserWa, { id: userWhatsapp[pos].id, socketId: idUserWa, channel: 'whatsapp' });
        } else {
            const data = await isHabilDay();
            timeStart = data.hourstart;
            timeStop = data.hourend;
            logger.info(
                "Horario de atención del día de hoy inicia;" +
                timeStart +
                " finalzia: " +
                timeStop
            );
            userWhatsapp[pos].bot = true;
            asignAgentWhat(idUserWa, {
                id: userWhatsapp[pos].id,
                socketId: idUserWa,
                channel: "whatsapp"
            });
        }
    }
    try {
        var mensajes = obj[0].queryResult.fulfillmentMessages;
        mensajes.forEach(async(e) => {
            if (e.platform === 'PLATFORM_UNSPECIFIED') {
                try {
                    let textWa = e.text.text[0];
                    if (textWa) {
                        try {
                            await tasksFunctions.saveMessage(
                                userWhatsapp[pos].jwt,
                                userWhatsapp[pos].id,
                                textWa,
                                "bot",
                            );
                        } catch (error) {
                            console.error(error);
                        }
                    }
                    utilMessages.sendMessage(
                        idUserWa, textWa
                    );
                } catch (error) {
                    logger.error(error);
                    logger.error('Error, intentando guardar mensaje');
                }
            }
        });
    } catch (error) {
        logger.error(error);
        logger.error('Error, no hay resultado del API de Dialogflow');
    }
}

//Envia mensajes del Asesor (BotHuman)
async function BotHuman(pos, idUserWa, messWa) {
    let sockidfor = userWhatsapp[pos].idSocket;
    userWhatsapp[pos].timeChat.reset()
    userWhatsapp[pos].timeChatFin.stop()
    try {
        await tasksFunctions.saveMessageAgent(
            userWhatsapp[pos].jwt,
            userWhatsapp[pos].id,
            userWhatsapp[pos].idCon,
            messWa,
            "user"
        )
        io.sockets.in(userWhatsapp[pos].idSocket).emit("private-message", {
            socketIdUser: userWhatsapp[pos].idSocket,
            messageUser: messWa,
            idCon: userWhatsapp[pos].idCon
        });
    } catch (error) {
        logger.error(error);
    }
}

async function BotHumanAudio(pos, idUserWa, messWa) {
    let sockidfor = userWhatsapp[pos].idSocket;
    userWhatsapp[pos].timeChat.reset()
    userWhatsapp[pos].timeChatFin.stop()
    try {
        await tasksFunctions.saveMessageAgent(
            userWhatsapp[pos].jwt,
            userWhatsapp[pos].id,
            userWhatsapp[pos].idCon,
            messWa,
            "user"
        )
        io.sockets.in(userWhatsapp[pos].idSocket).emit("private-message", {
            socketIdUser: userWhatsapp[pos].idSocket,
            messageUser: messWa,
            idCon: userWhatsapp[pos].idCon,
            audio: true
        });
    } catch (error) {
        logger.error(error);
    }
}
/*
  Metodo que valida el tipo de mensaje. Si es texto, si es una imagen, etc.
*/
async function handleMessage(message) {
    if (message.mimetype) {
        const mediaData = await decryptMedia(message);
        const imageBuffer = Buffer.from(mediaData.toString("base64"), "base64");
        const pathFile = `received-files/user-agent-${
      message.from
      }-${new Date().getHours()}${new Date().getMinutes()}${new Date().getSeconds()}.${
      message.mimetype.split("/")[1]
      }`;
        fs.writeFileSync(`src/public/${pathFile}`, imageBuffer);
        return pathFile;
    } else {
        return message.body;
    }
}

/*
Guardado de Usuario en Base de Datos
*/
async function checkandSave(result, idUserWa, nameWa) {
    if (result === undefined) {
        try {
            const data = await tasksFunctions.callUserWhatsapp(tokenAmd, idUserWa);

            if (data) {
                let idUs = data.id;
                const result = await tasksFunctions.reloadUserWa(tokenAmd, nameWa, idUserWa);
                userWhatsapp.push({
                    id: idUs,
                    name: nameWa,
                    idSocket: idUserWa,
                    idCon: 0,
                    jwt: result.token,
                    bot: false,
                    timeChat: new Timer(),
                    timeChatFin: new Timer()
                })
            } else {
                const resultOfSave = await tasksFunctions.saveUser(name = nameWa, acount = idUserWa);
                userWhatsapp.push({
                    id: resultOfSave.data[0].id,
                    name: nameWa,
                    idSocket: idUserWa,
                    idCon: 0,
                    jwt: resultOfSave.token,
                    bot: false,
                    timeChat: new Timer(),
                    timeChatFin: new Timer()
                })
            }
        } catch (error) {
            console.error(error);
        }
    }
}
/*
  envio de mensajes con cliente Sulla
*/
async function start(client, idUserWa, messWa, pathFile) {
    if (/\.(jpe?g|png|gif|bmp)$/i.test(pathFile)) {
        await client.sendImage(idUserWa, messWa);
    } else if (/\.(undefined|pdf)$/i.test(pathFile)) {
        messWa = messWa.replace(
            "data:application/octet-stream;base64,",
            "data:application/pdf;base64,"
        );
        await client.sendFile(idUserWa, messWa);
    } else {
        await client.sendText(idUserWa, messWa);
    }
}
/*
Busqueda de indice en array de usuarios Whatsapp
*/
function searchIndexWhatsapp(value) {
    const pos = userWhatsapp.findIndex(ele => ele.idSocket === value);
    return pos;
}
/*
  Asignamiento agente para Whatsapp
*/
async function asignAgentWhat(authorMess, payload) {
    let templates = await tasksFunctions.templates();
    let plant = [];
    templates.forEach((e) => {
        if (e.id === 3) {
            plant.push(e)
        }
    })
    var hour = convertToInt(nowHour());
    if (hour >= timeStart && hour <= timeStop) {
        var result = agentOnline.filter(soc => soc.state === 'ONLINE' && soc.connect === true && soc.numCon < soc.capacity);
        if (result === undefined || result.length === 0) {
            var author = authorMess;
            var text = plant[0].valor;
            chatsPendientes.push(payload)
            setTimeout(function() {
                utilMessages.sendMessage(author, text);
            }, 2500)

        } else {
            try {
                let x = findMinCon(result);
                io.sockets.in(result[x].idAgent).emit('event', payload, 'whatsapp');
                let pos = searchIndexAgent(result[x].idAgent);
                agentOnline[pos].numCon += 1;
            } catch (e) {
                logger.error('Error asignando conversacion al agente: ' + e);
            }
        }
    } else {
        var author = authorMess;
        let pos = searchIndexWhatsapp(authorMess)
        userWhatsapp[pos].bot = false;
        var text = plant[0].valor;
        setTimeout(function() {
            utilMessages.sendMessage(author, text);
        }, 2500)
    }
}
async function guardarAudio(url, path) {
    let response = await axios({
            method: 'GET',
            url: url,
            headers: {
                "Authorization": "Basic bGFpa2Fjb2xfV2E6TTBudDNjaDNsMCo=",
                'Accept': '*/*'
            },
            responseType: 'stream'
        })
        .then(res => {
            res.data.pipe(fs.createWriteStream(`src/public/sended-files/${path}.oga`));
            console.log("Archivo generado");
            return true;
        })
        .catch(err => {
            console.log(err);
            return false;
        })

    return response;
}

async function guardarDocumento(url, caption) {
    let response = await axios({
            method: 'GET',
            url: url,
            headers: {
                "Authorization": "Basic bGFpa2Fjb2xfV2E6TTBudDNjaDNsMCo=",
                'Accept': '*/*'
            },
            responseType: 'stream'
        })
        .then(res => {
            res.data.pipe(fs.createWriteStream(`src/public/sended-files/${caption}`));
            console.log("Archivo generado");
            return true;
        })
        .catch(err => {
            console.log(err);
            return false;
        })

    return response;
}
async function guardarVideo(url, path) {
    let response = await axios({
            method: 'GET',
            url: url,
            headers: {
                "Authorization": "Basic bGFpa2Fjb2xfV2E6TTBudDNjaDNsMCo=",
                'Accept': '*/*'
            },
            responseType: 'stream'
        })
        .then(res => {
            res.data.pipe(fs.createWriteStream(`src/public/sended-files/${path}.mp4`));
            console.log("Archivo generado");
            return true;
        })
        .catch(err => {
            console.log(err);
            return false;
        })

    return response;
}
async function guardarImagen(url, path) {
    let response = await axios({
            method: 'GET',
            url: url,
            headers: {
                "Authorization": "Basic bGFpa2Fjb2xfV2E6TTBudDNjaDNsMCo=",
                'Accept': '*/*'
            },
            responseType: 'stream'
        })
        .then(res => {
            res.data.pipe(fs.createWriteStream(`src/public/sended-files/${path}.jpeg`));
            console.log("Archivo generado");
            return true;
        })
        .catch(err => {
            console.log(err);
            return false;
        })

    return response;
}
/*****************************************************************/