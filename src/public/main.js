import io from "socket.io-client";
import { html, PolymerElement } from "@polymer/polymer/polymer-element.js";
import "@granite-elements/granite-bootstrap/granite-bootstrap.js";
import {} from "@polymer/polymer/lib/utils/resolve-url.js";
import "@polymer/paper-icon-button/paper-icon-button.js";
import "@polymer/paper-dialog/paper-dialog.js";
import "./css/style.css?name=polychat";
import "./css/chat.css?name=chatstyle";
import axios from "axios";
//variables para generación de hora actual y de id unico
const uuid = require("uuid");
/*
    ********************************************************************************************************************
    establecimiento de la variable socket para hacer uso de la función emit del modulo socket i.o
    ********************************************************************************************************************
*/
const pathUrl = "https://chatbots.montechelo.com.co";
const nomlocalStor = "data_user_bot_laika";
const nomIdConStor = "idCon_laika";
const nomCountStor = "count_laika";
const socket = io.connect(pathUrl, {
    path: "/widget/laika/socket.io",
    forceNew: true
});
socket.off("private-message");
socket.off("new-message");

var count = 0;
/*
    ********************************************************************************************************************
    Clase que extiende del PolymerElement, define el html del custom element web-chat, sus propiedades y functiones
    ********************************************************************************************************************
*/
class WebChat extends PolymerElement {
    constructor() {
        super();
        this.attachShadow({ mode: "open", delegatesFocus: false });
    }

    static get template() {
        return html `
    <style include = "polychat"></style>
    
    <section class="avenue-messenger">
      <div class="menu">
      <div class="items"><span>
        <a href="#" title="Minimize">&mdash;</a><br>
        <a href="#" title="End Chat">&#10005;</a>
        
        </span></div>
        <div on-click="showHide" class="button"><img width="15px" height="15px" src="{{imgclosewindow}}" /></div>
      </div>
    <div class="chat">        
      <div class="chat-title">
        <img style="padding-bottom:15px;" src="{{imglaika}}" />
      </div>
      <div class="messages">

        <div class="messages-content" id="boxMess"></div>
      </div>
      <div class="message-box">
      <textarea on-click="downWin" id="private_message" type="text" class="message-input" placeholder="Escribe tu mensaje..." on-keydown="sendMessage"></textarea>
      <label class="message-submit-clip" for="file">
        <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-paperclip" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M4.5 3a2.5 2.5 0 0 1 5 0v9a1.5 1.5 0 0 1-3 0V5a.5.5 0 0 1 1 0v7a.5.5 0 0 0 1 0V3a1.5 1.5 0 1 0-3 0v9a2.5 2.5 0 0 0 5 0V5a.5.5 0 0 1 1 0v7a3.5 3.5 0 1 1-7 0V3z"/>
        </svg>
      </label>
      <input type="file" id="file" on-change="sendFile">
      <button type="submit" class="message-submit-menu" on-click="sendMessageMenu">MENÚ</button>
        <button type="submit" class="message-submit" on-click="sendMessage"><img width="30px" src="{{imgsrcbtn}}"></button>
      </div>
      <div class="fotstyle">
      <p class="pstyle">Powered By <a style="text-decoration: none !important; color:#FFFFFF; " href="https://www.montechelo.com.co/" target="_blank">Montechelo</a></p>
      </div>
    </div>
      </div>
    `;
    }

    static get properties() {
        return {
            list: {
                type: Array,
                value: []
            },
            imgsrc: {
                type: String,
                value: function() {
                    return this.resolveUrl(pathUrl + "/widget/laika/img/boticon.png");
                }
            },
            imgsrcbtn: {
                type: String,
                value: function() {
                    return this.resolveUrl(pathUrl + "/widget/laika/img/send.svg");
                }
            },
            imglaika: {
                type: String,
                value: function() {
                    return this.resolveUrl(pathUrl + "/widget/laika/img/logo.png");
                }
            },
            imgiconbot: {
                type: String,
                value: function() {
                    return this.resolveUrl(pathUrl + "/widget/laika/img/icon-agent.svg");
                }
            },
            imgactive: {
                type: String,
                value: function() {
                    return this.resolveUrl(
                        pathUrl + "/widget/laika/img/status-active.svg"
                    );
                }
            },
            imgchatbot: {
                type: String,
                value: function() {
                    return this.resolveUrl(pathUrl + "/widget/laika/img/icon-agent.svg");
                }
            },
            imgchatuser: {
                type: String,
                value: function() {
                    return this.resolveUrl(pathUrl + "/widget/laika/img/icon-user.svg");
                }
            },
            imgclosewindow: {
                type: String,
                value: function() {
                    return this.resolveUrl(pathUrl + "/widget/laika/img/close.png");
                }
            }
        };
    }

    ready() {
        super.ready();
        this.runOncePerDay();

        window.mess = this.$.messages;
        window.boxMess = this.$.boxMess;
        window.divv = this.$.winMessages;

        window.prvmess = this.$.private_message;
        //Si usuario ya se encuentra registrado
        if (JSON.parse(localStorage.getItem(nomlocalStor))) {
            window.idUser = JSON.parse(localStorage.getItem(nomlocalStor)).id;
            window.idToken = JSON.parse(localStorage.getItem(nomlocalStor)).token;
            window.idTokenJWT = JSON.parse(localStorage.getItem(nomlocalStor)).jwt;

            axios
                .post(
                    pathUrl + "/widget/laika/tasks/callAllConversation", {
                        idUser: window.idUser
                    }, {
                        headers: {
                            "Content-Type": "application/json",
                            authorization: "Bearer " + window.idTokenJWT
                        }
                    }
                )
                .then(res => {
                    res.data.forEach(e => {
                        let node = document.createElement("div");
                        if (e.who == "user") {
                            renderMessageUser(node, e.message, e.hora);
                        } else if (e.who == "bot") {
                            renderMessageBot(node, e.message, e.hora);
                        } else if (e.who == "agent") {
                            renderMessageAgent(node, e.message, e.hora);
                        }
                    });
                    window.boxMess.scrollTop = window.boxMess.scrollHeight;
                });

            if (localStorage.getItem(nomIdConStor)) {
                window.idConversation = localStorage.getItem(nomIdConStor);
                axios
                    .post(
                        pathUrl + "/widget/laika/tasks/statusConvertation", {
                            idCon: localStorage.getItem(nomIdConStor)
                        }, {
                            headers: {
                                "Content-Type": "application/json",
                                authorization: "Bearer " + window.idTokenJWT
                            }
                        }
                    )
                    .then(res => {
                        if (res.data[0].estado != "Abierta") {
                            localStorage.setItem(nomCountStor, 0);
                            localStorage.removeItem(nomIdConStor);
                        }
                    });
            }
        }

        if (parseInt(localStorage.getItem(nomCountStor)) == 3) {
            socket.emit(
                "Agent-User",
                JSON.parse(localStorage.getItem(nomlocalStor)).token,
                null,
                null,
                null,
                "webchat",
                "user"
            );
        }
    }
    hasOneDayPassed() {
        // get today's date. eg: "7/37/2007"
        var date = new Date().toLocaleDateString();
        // if there's a date in localstorage and it's equal to the above:
        // inferring a day has yet to pass since both dates are equal.
        if (localStorage.getItem("date_app_laika") == date) return false;

        // this portion of logic occurs when a day has passed
        localStorage.setItem("date_app_laika", date);
        return true;
    }

    // some function which should run once a day
    runOncePerDay() {
        if (!this.hasOneDayPassed()) return false;
        localStorage.removeItem(nomlocalStor);
        localStorage.removeItem(nomCountStor);
        localStorage.removeItem(nomIdConStor);
    }
    _isEqual(list, string) {
        return list == string;
    }
    downWin() {
        window.boxMess.scrollTop = window.boxMess.scrollHeight;
    }
    reset() {
        localStorage.setItem(nomCountStor, 0);
    }
    sendMessageMenu() {

        if (localStorage.getItem(nomCountStor) < 3) {
            if (window.nodoSuggestions != undefined) {
                try {
                    window.boxMess.removeChild(window.nodoSuggestions);
                    window.nodoSuggestions = undefined;
                } catch (e) {
                    console.error(e);
                }
            }
            var node = document.createElement("div");
            renderMessageUser(node, "Menú", nowHour());
            socket.emit("new-message", {
                message: "menú",
                id: JSON.parse(localStorage.getItem(nomlocalStor)).token
            });
        }
    }
    sendFile(evt) {
        const data = evt.target.files[0];
        const n = data.name;



        const payload = 'Esto es un archivo';
        const exten = n.split('.').pop();

        let node = document.createElement("div");
        renderMessageUserFile(node, evt, exten, nowHour());

        if (
            localStorage.getItem(nomCountStor) == "0" ||
            localStorage.getItem(nomCountStor) == "1"
        ) {
            socket.emit("new-message", {
                message: payload,
                pathFile: data,
                id: JSON.parse(localStorage.getItem(nomlocalStor)).token
            });
        } else if (localStorage.getItem(nomCountStor) == "2") {
            socket.emit("Agent", "Chats", {
                socketId: JSON.parse(localStorage.getItem(nomlocalStor)).token,
                id: window.idUser,
                channel: "webchat"
            });
            var node2 = document.createElement("div");
            renderMessageBot(
                node2,
                "En un momento un agente se comunicara contigo",
                nowHour()
            );
            count = parseInt(localStorage.getItem(nomCountStor)) + 1;
            localStorage.removeItem(nomCountStor);
            localStorage.setItem(nomCountStor, count);
            socket.emit(
                "Agent-User",
                JSON.parse(localStorage.getItem(nomlocalStor)).token,
                null
            );
        } else {

            const reader = new FileReader();
            reader.readAsDataURL(data);
            reader.onload = function() {

                socket.emit(
                    "Agent-User",
                    JSON.parse(localStorage.getItem(nomlocalStor)).token,
                    reader.result,
                    null,
                    window.idConversation,
                    "webchat",
                    "tmoReset",
                    n
                );
            }
        }
        this.$.private_message.value = "";
        window.boxMess.scrollTop = window.boxMess.scrollHeight;
    }
    sendMessage(evt) {
        var payload = this.$.private_message.value;
        if ((evt.keyCode == 13 || evt.type == "click") && payload.trim() != "") {
            evt.preventDefault();
            if (window.nodoSuggestions != undefined) {
                try {
                    window.boxMess.removeChild(window.nodoSuggestions);
                    window.nodoSuggestions = undefined;
                } catch (e) {
                    console.error(e);
                }
            }
            var node = document.createElement("div");
            renderMessageUser(node, payload, nowHour());
            if (
                localStorage.getItem(nomCountStor) == "0" ||
                localStorage.getItem(nomCountStor) == "1"
            ) {
                socket.emit("new-message", {
                    message: payload,
                    id: JSON.parse(localStorage.getItem(nomlocalStor)).token
                });
                axios
                    .post(
                        pathUrl + "/widget/laika/tasks/saveMessage", {
                            idUser: window.idUser,
                            message: payload,
                            from: "user"
                        }, {
                            headers: {
                                "Content-Type": "application/json",
                                authorization: "Bearer " + window.idTokenJWT
                            }
                        }
                    )
                    .then(() => {
                        this.downWin();
                    });
            } else if (localStorage.getItem(nomCountStor) == "2") {
                socket.emit("Agent", "Chats", {
                    socketId: JSON.parse(localStorage.getItem(nomlocalStor)).token,
                    id: window.idUser,
                    channel: "webchat"
                });
                axios
                    .post(
                        pathUrl + "/widget/laika/tasks/saveMessage", {
                            idUser: window.idUser,
                            message: payload,
                            from: "user"
                        }, {
                            headers: {
                                "Content-Type": "application/json",
                                authorization: "Bearer " + window.idTokenJWT
                            }
                        }
                    )
                    .then(() => {
                        this.downWin();
                    });
                var node2 = document.createElement("div");
                renderMessageBot(
                    node2,
                    "En un momento un agente se comunicara contigo",
                    nowHour()
                );
                count = parseInt(localStorage.getItem(nomCountStor)) + 1;
                localStorage.removeItem(nomCountStor);
                localStorage.setItem(nomCountStor, count);
                socket.emit(
                    "Agent-User",
                    JSON.parse(localStorage.getItem(nomlocalStor)).token,
                    null
                );
            } else {
                socket.emit(
                    "Agent-User",
                    JSON.parse(localStorage.getItem(nomlocalStor)).token,
                    payload,
                    null,
                    window.idConversation,
                    "webchat",
                    "tmoReset"
                );
                axios
                    .post(
                        pathUrl + "/widget/laika/tasks/saveMessageAgent", {
                            idUser: window.idUser,
                            idSocket: window.idConversation,
                            message: payload,
                            from: "user"
                        }, {
                            headers: {
                                "Content-Type": "application/json",
                                authorization: "Bearer " + window.idTokenJWT
                            }
                        }
                    )
                    .then(() => {
                        this.downWin();
                    });
            }
            this.$.private_message.value = "";
            window.boxMess.scrollTop = window.boxMess.scrollHeight;
        }
    }
    showHide() {
        var displayed = window.cchat2.style.display;
        window.segundo.style.visibility = "hidden";
        window.segundo.style.display = "none";
        window.primero.style.visibility = "visible";
        window.primero.style.display = "block";
        if (displayed == "none") {
            window.cchat2.style.display = "";
        } else {
            window.cchat2.style.display = "none";
        }
    }
}
/*
    ********************************************************************************************************************
    Clase que extiende del PolymerElement, define el html del custom element web-form, sus propiedades y functiones
    ********************************************************************************************************************
*/
class WebForm extends PolymerElement {
    constructor() {
        super();
        this.attachShadow({ mode: "open", delegatesFocus: false });
    }

    static get template() {
        return html `
    <style include = "polychat">
    </style>
    <section class="avenue-messenger">
      <div class="menu">
      <div class="items"><span>
        <a href="#" title="Minimize">&mdash;</a><br>
        <a href="#" title="End Chat">&#10005;</a>        
        </span></div>
        <div on-click="showHide" class="button"><img width="15px" height="15px" src="{{imgclosewindow}}" /></div>
      </div>
    <div class="chat">
      <div class="chat-title">
      <img style="padding-bottom:15px;" src="{{imglaika}}" />
      </div>
      <div class="messages">
      <!--inicio form-->
      <paper-dialog id="modalTermsCon" class="size-modalTerms">
        <div style="text-align:justify; text-justify: inter-word;">
        <p style="text-align: center;"><strong>Tratamiento de datos personales</strong></p><br>
        En cumplimiento de la Ley 1581 de 2012, por la cual se dictan disposiciones para la protección de datos personales, laika Inversiones SAS en su calidad de responsable del tratamiento de datos personales, informa los lineamientos generales en esta materia.<br><br>
        Se comunica a los interesados que los datos facilitados en el presente formulario van a ser incorporados en nuestra base de candidatos cuyo responsable es laika Inversiones SAS.<br><br>
        En caso de que no se desease que laika Inversiones SAS tuviera un dato suyo o si quisiera tener conocimiento sobre los datos que tenemos sobre usted o si desea modificarlos o, en su caso, que los cancelemos de la base de datos, deberá enviar un correo electrónico a <span style="color:#009BC2">protecciondedatos@laika.com.co </span> indicando e intención excluir los datos de las bases de laika Inversiones SAS  junto con sus datos (Nombre, Cédula y Teléfono)
        </div>
        <div class="buttons">
          <input class="buttonClose" type="button" on-click="hideModalTermsCon" value="Cerrar">         
        </div>
      </paper-dialog>
      <paper-dialog id="modalCorreo" class="size-modalData">
        <p class="paragraphModal">Debes ingresar un <strong>correo eléctronico</strong> valido</p>
        <div class="buttons">
          <input class="buttonClose" type="button" on-click="hideModalEmail" value="Cerrar">         
        </div>
      </paper-dialog>
      <paper-dialog id="modalData" class="size-modalData">
        <p class="paragraphModal">Por favor, dime tu <strong>nombre</strong> y <strong>correo eléctronico</strong></p>
        <div class="buttons">
          <input class="buttonClose" type="button" on-click="hideModal" value="Cerrar">         
        </div>
      </paper-dialog>
      <paper-dialog id="modalTerms" class="size-modalData">
        <p class="paragraphModal">Para continuar con nuestro servicio debes aceptar <strong>términos y condiciones</strong> </p>
        <div class="buttons">
          <input class="buttonClose" type="button" on-click="hideModalTerms" value="Cerrar">         
        </div>
      </paper-dialog>
        <div class="messages-content">
        <img src="{{imgactive}}" class="imgstyle2"/>
          <img src="{{imgiconbot}}" class="imgstyle"/>
          <p class="greetings">¡Hola!</p>
          <p class="paragraph">Mi nombre es <strong>Monty</strong>, Por favor <strong>completa la siguiente información</strong> para que charlemos.</p>
          <br/>
          <div class="divForm" style="">
            <form class="formFlex">
              <label for="femail" class="labelForm">Email</label>
              <input type="email" id="email" name="email" class="inputclass" placeholder="Escribe tu email">
              <label for="fname" class="labelForm">Nombre</label>
              <input type="text" id="name" name="name" class="inputclass" placeholder="Escribe tu nombre">
            </form>
              <input id="defaultCheck1" class="check" type="checkbox" name="terminos" value="terminos"><span  on-click="openTerms" style="cursor: pointer; color: #009BC2;"> Aceptar <span style="text-decoration:underline;">terminos y condiciones</span></span><br>
              <input class="buttonContinue" type="button" on-click="submit" value="Continuar">            
          </div>
        </div>
      </div>
      <!--fin form -->
      <div class="fotstyle">
        <p class="pstyle">Powered By <a style="text-decoration: none !important; color:#FFFFFF; " href="https://www.montechelo.com.co/" target="_blank">Montechelo</a></p>
      </div>
    </div>
      </div>
    `;
    }

    static get properties() {
        return {
            imglaika: {
                type: String,
                value: function() {
                    return this.resolveUrl(pathUrl + "/widget/laika/img/logo.png");
                }
            },
            imgiconbot: {
                type: String,
                value: function() {
                    return this.resolveUrl(pathUrl + "/widget/laika/img/icon-agent.svg");
                }
            },
            imgactive: {
                type: String,
                value: function() {
                    return this.resolveUrl(
                        pathUrl + "/widget/laika/img/status-active.svg"
                    );
                }
            },
            imgclosewindow: {
                type: String,
                value: function() {
                    return this.resolveUrl(pathUrl + "/widget/laika/img/close.png");
                }
            }
        };
    }

    ready() {
        super.ready();
    }

    showHide() {
        window.segundo.style.visibility = "hidden";
        window.segundo.style.display = "none";
        window.primero.style.visibility = "visible";
        window.primero.style.display = "block";
        var displayed = window.cchat.style.display;
        if (displayed == "none") {
            window.cchat.style.display = "";
        } else {
            window.cchat.style.display = "none";
        }
    }
    openTerms() {
        this.$.modalTermsCon.open();
    }

    hideModalTermsCon() {
        this.$.modalTermsCon.toggle();
    }

    hideModal() {
        this.$.modalData.toggle();
    }

    hideModalTerms() {
        this.$.modalTerms.toggle();
    }
    hideModalEmail() {
        this.$.modalCorreo.toggle();
    }

    validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    submit(e) {
        e.preventDefault();

        var nameUser = this.$.name.value;
        var emailUser = this.$.email.value;
        var terminosUser = this.$.defaultCheck1.checked;

        if (nameUser && emailUser && terminosUser === true) {
            if (this.validateEmail(emailUser) === false) {
                this.$.modalCorreo.open();
            } else {
                axios
                    .post(pathUrl + "/widget/laika/tasks/saveUser", {
                        name: nameUser,
                        email: emailUser,
                        conditions: "Aceptado"
                    })
                    .then(response => {
                        localStorage.setItem(
                            nomlocalStor,
                            JSON.stringify({
                                id: response.data.data[0].id,
                                name: nameUser,
                                email: emailUser,
                                token: uuid() + getCleanedString(nameUser),
                                jwt: response.data.token
                            })
                        );
                        localStorage.setItem(nomCountStor, 0);
                        window.idUser = response.data.data[0].id;
                        window.idTokenJWT = response.data.token;
                        window.cchat.style.display = "none";
                        socket.emit("new-message", {
                            message: "Hola",
                            id: JSON.parse(localStorage.getItem(nomlocalStor)).token
                        });
                        window.cchat2.style.display = "";
                    })
                    .catch(e => {
                        console.error(e);
                    });
            }
        } else if (!nameUser) {
            this.$.modalData.open();
        } else if (!emailUser) {
            this.$.modalData.open();
        } else if (terminosUser === false) {
            this.$.modalTerms.open();
        }
    }
}

/*
    ********************************************************************************************************************
    Clase que extiende del PolymerElement, define el html del custom element web-icon, sus propiedades y functiones
    ********************************************************************************************************************
*/
class WebIcon extends PolymerElement {
    constructor() {
        super();
        this.attachShadow({ mode: "open", delegatesFocus: false });
    }

    static get template() {
        return html `
      <style>
        .buttonChat {
          position: fixed;
          bottom: 10px;
          right: 10px;
          border: none;
          color: white;
          text-align: center;
          text-decoration: none;
          display: inline-block;
          font-size: 16px;
          margin: 80px 2px;
          cursor: pointer;
          z-index: 999;
        }
      </style>
      <style include="granite-bootstrap"></style>
      <style include="chatstyle"></style>

      <web-form
        id="chatWin"
        style="border-radius: 50px 20px; display:none"
      ></web-form>
      <web-chat
        id="chatWin2"
        style="border-radius: 50px 20px; display:none"
      ></web-chat>

      <div class="buttonChat bubble">
        <p
          id="primero"
          style="visibility:visible; display:block;"
          on-click="visualiza_segundo"
        >
          <img src="{{imgsrchat}}" style="width:30px; margin-top:10px;" />
        </p>
        <p
          id="segundo"
          style="visibility:hidden; display:none;"
          on-click="visualiza_primero"
        >
          <img src="{{imgsrclose}}" style="width:25px; margin-top:18px;" />
        </p>
      </div>
    `;
    }

    ready() {
        super.ready();
        window.cchat = this.$.chatWin;
        window.cchat2 = this.$.chatWin2;
        window.primero = this.$.primero;
        window.segundo = this.$.segundo;
    }
    static get properties() {
        return {
            imgsrchat: {
                type: String,
                value: function() {
                    return this.resolveUrl(pathUrl + "/widget/laika/img/chat.svg");
                }
            },
            imgsrclose: {
                type: String,
                value: function() {
                    return this.resolveUrl(pathUrl + "/widget/laika/img/close.png");
                }
            }
        };
    }

    visualiza_primero() {
        this.$.primero.style.visibility = "visible";
        this.$.primero.style.display = "block";
        this.$.segundo.style.visibility = "hidden";
        this.$.segundo.style.display = "none";
        window.boxMess.scrollTop = window.boxMess.scrollHeight;
        if (localStorage.getItem(nomlocalStor)) {
            var displayed = this.$.chatWin2.style.display;
            if (displayed == "none") {
                this.$.chatWin2.style.display = "";
            } else {
                this.$.chatWin2.style.display = "none";
            }
            window.boxMess.scrollTop = window.boxMess.scrollHeight;
        } else {
            var displayed = this.$.chatWin.style.display;
            if (displayed == "none") {
                this.$.chatWin.style.display = "";
            } else {
                this.$.chatWin.style.display = "none";
            }
            window.boxMess.scrollTop = window.boxMess.scrollHeight;
        }
    }
    visualiza_segundo() {
        this.$.segundo.style.visibility = "visible";
        this.$.segundo.style.display = "block";
        this.$.primero.style.visibility = "hidden";
        this.$.primero.style.display = "none";
        if (localStorage.getItem(nomlocalStor)) {
            var displayed = this.$.chatWin2.style.display;
            if (displayed == "none") {
                this.$.chatWin2.style.display = "";
            } else {
                this.$.chatWin2.style.display = "none";
            }
        } else {
            var displayed = this.$.chatWin.style.display;
            if (displayed == "none") {
                this.$.chatWin.style.display = "";
            } else {
                this.$.chatWin.style.display = "none";
            }
        }
    }
}

/*
    ********************************************************************************************************************
    Definición de los elementos personalizados (custom-element) en el navegador
    ********************************************************************************************************************
*/
window.customElements.define("web-icon", WebIcon);
window.customElements.define("web-chat", WebChat);
window.customElements.define("web-form", WebForm);
/*
    ********************************************************************************************************************
    suscripción del cliente al canal new-message del socket, permite la comunicación del cliente con el usuario
    ********************************************************************************************************************
*/
socket.on("new-message", async function(data) {
    var node = document.createElement("div");

    if (data.action != "suggestions") {
        window.setTimeSugg = 3000;
    } else {
        window.setTimeSugg = 0;
    }

    if (data.action == "human") {
        window.setTimeMsg = 0;
    } else if (data.action == "resetChat") {
        localStorage.setItem(nomCountStor, 0);
        window.setTimeMsg = 2200;
    } else {
        window.setTimeMsg = 2200;
    }

    if (data.msg) {
        renderLoadingMsg(node);
        setTimeout(function() {
            renderMessageBot(node, data.msg, nowHour());
            axios.post(
                pathUrl + "/widget/laika/tasks/saveMessage", {
                    idUser: window.idUser,
                    message: data.msg,
                    from: "bot"
                }, {
                    headers: {
                        "Content-Type": "application/json",
                        authorization: "Bearer " + window.idTokenJWT
                    }
                }
            );
        }, window.setTimeMsg);
    } else if (data.sug) {
        setTimeout(function() {
            if (window.nodoSuggestions != undefined) {
                try {
                    window.boxMess.removeChild(window.nodoSuggestions);
                    window.nodoSuggestions = undefined;
                } catch (e) {
                    console.error(e);
                }
            }
            window.nodoSuggestions = document.createElement("div");
            window.nodoSuggestions.setAttribute(
                "class",
                "server-response-suggestions"
            );
            window.nodoSuggestions.setAttribute("id", "suggestion");
            window.boxMess.appendChild(window.nodoSuggestions);
            data.sug.forEach(function(e) {
                var node1 = document.createElement("button");
                node1.setAttribute("class", "sug");
                node1.setAttribute("value", "" + e.title + "");
                node1.innerHTML = e.title;
                window.nodoSuggestions.appendChild(node1);
                window.boxMess.scrollTop = window.boxMess.scrollHeight;
                node1.addEventListener("click", function() {
                    node.setAttribute("class", "message message-personal");
                    var messugestion = node1.getAttribute("value");
                    axios.post(
                        pathUrl + "/widget/laika/tasks/saveMessage", {
                            idUser: window.idUser,
                            message: messugestion,
                            from: "user"
                        }, {
                            headers: {
                                "Content-Type": "application/json",
                                authorization: "Bearer " + window.idTokenJWT
                            }
                        }
                    );
                    renderMessageUser(node, messugestion, nowHour());
                    window.boxMess.removeChild(window.nodoSuggestions);
                    window.nodoSuggestions = undefined;
                    socket.emit("new-message", {
                        message: node1.getAttribute("value"),
                        id: JSON.parse(localStorage.getItem(nomlocalStor)).token
                    });
                    window.prvmess.value = "";
                    window.boxMess.scrollTop = window.boxMess.scrollHeight;
                });
            });
        }, window.setTimeSugg);
    }
});

socket.on("estado", function(msg) {
    if (msg == "nuevo") {
        count = parseInt(localStorage.getItem(nomCountStor)) + 1;
        localStorage.removeItem(nomCountStor);
        localStorage.setItem(nomCountStor, count);
    } else {
        socket.emit("Agent", "Chats", {
            socketId: JSON.parse(localStorage.getItem(nomlocalStor)).token,
            id: window.idUser,
            channel: "webchat"
        });
        localStorage.removeItem(nomCountStor);
        localStorage.setItem(nomCountStor, 3);
        socket.emit(
            "Agent-User",
            JSON.parse(localStorage.getItem(nomlocalStor)).token,
            null
        );
    }
});

socket.on("private-message", function(data) {
    var node = document.createElement("div");
    if (data.messageUser != null) {
        if (data.state == null) {
            socket.emit(
                "Agent-User",
                JSON.parse(localStorage.getItem(nomlocalStor)).token,
                null,
                null,
                null,
                "webchat",
                "tmoReset"
            );
            renderMessageAgent(node, data.messageUser, nowHour());
        } else if (data.state == 1 || data.state == 5) {
            axios.post(
                pathUrl + "/widget/laika/tasks/saveMessageAgent", {
                    idUser: window.idUser,
                    idSocket: JSON.parse(localStorage.getItem(nomIdConStor)),
                    message: data.messageUser,
                    from: "agent"
                }, {
                    headers: {
                        "Content-Type": "application/json",
                        authorization: "Bearer " + window.idTokenJWT
                    }
                }
            );
            renderMessageAgent(node, data.messageUser, nowHour());
            localStorage.removeItem(nomCountStor);
            localStorage.removeItem(nomIdConStor);
            localStorage.setItem(nomCountStor, 0);
            socket.emit(
                "Agent-User",
                JSON.parse(localStorage.getItem(nomlocalStor)).token,
                null,
                null,
                window.idConversation,
                "webchat",
                "tmoFin"
            );
        } else if (data.state == 2) {
            axios.post(
                pathUrl + "/widget/laika/tasks/saveMessageAgent", {
                    idUser: window.idUser,
                    idSocket: data.idCon,
                    message: data.messageUser,
                    from: "agent"
                }, {
                    headers: {
                        "Content-Type": "application/json",
                        authorization: "Bearer " + window.idTokenJWT
                    }
                }
            );
            renderMessageAgent(node, data.messageUser, nowHour());
            localStorage.setItem(nomIdConStor, data.idCon);
            window.idConversation = data.idCon;
            socket.emit(
                "Agent-User",
                JSON.parse(localStorage.getItem(nomlocalStor)).token,
                null,
                null,
                window.idConversation,
                "webchat",
                "tmo"
            );
        } else if (data.state == 4) {
            axios.post(
                pathUrl + "/widget/laika/tasks/saveMessageAgent", {
                    idUser: window.idUser,
                    idSocket: data.idCon,
                    message: data.messageUser,
                    from: "agent"
                }, {
                    headers: {
                        "Content-Type": "application/json",
                        authorization: "Bearer " + window.idTokenJWT
                    }
                }
            );
            renderMessageAgent(node, data.messageUser, nowHour());
        }
    }
});

/*
  Function para renderizado de archivos
*/
function renderMessageUserFile(node, evt, type, hour) {

    const typeLC = type.toLowerCase()
    let reader = new FileReader();
    const condition = (typeLC == 'png') || (typeLC == 'jpg') || (typeLC == 'svg') || (typeLC == 'gif') || (typeLC == 'jpeg');

    if (condition) {
        node.setAttribute("class", "message message-personal");

        let file = evt.target.files[0];

        reader.readAsDataURL(file);

        const image = new Image();

        reader.onload = function(evt) {
            image.src = evt.target.result;
            image.width = '60';
        }


        node.innerHTML =
            "<figure class='avatarUser'><img src='https://chatbots.montechelo.com.co/widget/laika/img/icon-user.svg'></figure>";
        node.appendChild(image);


        window.boxMess.appendChild(node);
        window.boxMess.scrollTop = window.boxMess.scrollHeight;
    } else {

        node.setAttribute("class", "message message-personal");
        let file = evt.target.files[0];
        let documentName = file.name;
        const a = document.createElement('a');

        reader.readAsDataURL(file);
        reader.onload = function() {
            a.href = reader.result;
        }

        let documentImage = new Image();
        documentImage.src = 'https://chatbots.montechelo.com.co/widget/laika/img/download.png';
        documentImage.width = 80;
        documentImage.title = documentName;
        a.appendChild(documentImage);

        node.innerHTML =
            "<figure class='avatarUser'><img src='https://chatbots.montechelo.com.co/widget/laika/img/icon-user.svg'></figure>" +
            "<p class='putTimestampUser'>" +
            hour +
            "</p>";
        node.appendChild(a);


        window.boxMess.appendChild(node);
        window.boxMess.scrollTop = window.boxMess.scrollHeight;
    }


}

/*
  Function para renderizado de mensajes
*/
function renderMessageUser(node, msg, hour) {
    node.setAttribute("class", "message message-personal");
    if (/\.(jpe?g|png|gif|bmp)$/i.test(msg)) {
        node.innerHTML =
            "<figure class='avatarUser'><img src='https://chatbots.montechelo.com.co/widget/laika/img/icon-user.svg'></figure>" +
            `<a href="${msg}" target="_blank"><img src="${msg}" style="width:60%;"/><br></a>` +
            "<p class='putTimestampUser'>" +
            hour +
            "</p>";
    } else if (/\.(undefined|pdf)$/i.test(msg)) {
        node.innerHTML =
            "<figure class='avatarUser'><img src='https://chatbots.montechelo.com.co/widget/laika/img/icon-user.svg'></figure>" +
            `<a href="${msg}" download>
          Archivo 
          <img src="https://chatbots.montechelo.com.co/widget/laika/img/download.png" width="30" height="30">
      </a>`; +
        "<p class='putTimestampUser'>" + hour + "</p>";
    } else {
        var urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gi;
        let strNew = msg.replace(urlRegex, function(url) {
            return '<a href="' + url + '" style="color: var(--secondary-color);">' + url + "</a>";
        });
        node.innerHTML =
            "<figure class='avatarUser'><img src='https://chatbots.montechelo.com.co/widget/laika/img/icon-user.svg'></figure>" +
            strNew +
            "<p class='putTimestampUser'>" +
            hour +
            "</p>";
    }
    window.boxMess.appendChild(node);
    window.boxMess.scrollTop = window.boxMess.scrollHeight;
}
/*
  Mensaje Loading
*/
function renderLoadingMsg(node) {
    node.setAttribute("class", "message new loading");
    node.innerHTML =
        "<figure class='avatar'><img src='https://chatbots.montechelo.com.co/widget/laika/img/icon-agent.svg'></figure><span></span><span></span><span></span><p class='putTimestampBot'>" +
        nowHour() +
        "</p>";
    window.boxMess.appendChild(node);
    window.boxMess.scrollTop = window.boxMess.scrollHeight;
}
/*
  Funcion para rendedizar mensaje del bot
*/
function renderMessageBot(node, msg, hour) {
    var strNew = msg.replace(/\*(\S(.*?\S)?)\*/gm, "<strong>$1</strong>");
    var strNews = strNew.replace(/(?:\r\n|\r|\n)/g, "<br>");
    let strLink = strNews.replace(
        /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gm,
        function(url) {
            return '<a target="autoblank" href="' + url + '">' + url + "</a>";
        }
    );
    node.setAttribute("class", "message new");
    node.innerHTML =
        "<figure class='avatar'><img src='https://chatbots.montechelo.com.co/widget/laika/img/icon-agent.svg'></figure>" +
        strLink +
        "<p class='putTimestampBot'>" +
        hour +
        "</p>";
    window.boxMess.appendChild(node);
    window.boxMess.scrollTop = window.boxMess.scrollHeight;
}
/*
/*
  Funcion para rendedizar mensaje del Agente
*/
function renderMessageAgent(node, msg, hour) {
    console.log('Entra aca')
    var node = document.createElement("div");
    node.setAttribute("class", "message new");
    if (/\.(jpe?g|png|gif|bmp)$/i.test(msg)) {
        node.innerHTML =
            "<figure class='avatar'><img src='https://chatbots.montechelo.com.co/widget/laika/img/icon-agent.svg'></figure>" +
            `<a href="${msg}" target="_blank"><img src="${msg}" style="width:60%;"/><br></a>` +
            "<p class='putTimestampUser'>" +
            hour +
            "</p>";
    } else if (/\.(undefined|pdf)$/i.test(msg)) {
        node.innerHTML =
            "<figure class='avatar'><img src='https://chatbots.montechelo.com.co/widget/laika/img/icon-agent.svg'></figure>" +
            `<a href="${msg}" download>
          Archivo 
          <img src="https://chatbots.montechelo.com.co/widget/laika/img/download.png" width="30" height="30">
      </a>`; +
        "<p class='putTimestampUser'>" + hour + "</p>";
    } else if (/\.(mp4|avi|wmv|mpg|mov|ogg)$/i.test(msg)) {
        node.innerHTML =
            "<figure class='avatar'><img src='https://chatbots.montechelo.com.co/widget/laika/img/icon-agent.svg'></figure>" +
            `<a href="${msg}" download>
          Archivo 
          <img src="https://chatbots.montechelo.com.co/widget/laika/img/videoDown.png" width="30" height="30">
      </a>`; +
        "<p class='putTimestampUser'>" + hour + "</p>";
    } else {
        var urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gi;
        let strNew = msg.replace(urlRegex, function(url) {
            return '<a href="' + url + '" style="color: #FFF;">' + url + "</a>";
        });
        node.innerHTML =
            "<figure class='avatar'><img src='https://chatbots.montechelo.com.co/widget/laika/img/icon-agent.svg'></figure>" +
            strNew +
            "<p class='putTimestampUser'>" +
            hour +
            "</p>";
    }
    window.boxMess.appendChild(node);
    window.boxMess.scrollTop = window.boxMess.scrollHeight;
}
/*
  Funcion para obtener formato de hora-minutos-segundos hh:mm:ss
*/
function nowHour() {
    var date = new Date();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    var apm = " ";
    if (hours < 10) hours = "0" + hours;
    if (minutes < 10) minutes = "0" + minutes;
    if (hours > 12) {
        hours = hours - 12;
        apm = "pm";
    } else if (hours == 12) {
        apm = "pm";
    } else {
        apm = "am";
    }
    if (seconds < 10) seconds = "0" + seconds;
    return hours + ":" + minutes + ":" + seconds + " " + apm;
}

function getCleanedString(cadena) {
    // Definimos los caracteres que queremos eliminar
    var specialChars = "!@#$^&%*()+=-[]/{}|:<>?,.";

    // Los eliminamos todos
    for (var i = 0; i < specialChars.length; i++) {
        cadena = cadena.replace(new RegExp("\\" + specialChars[i], "gi"), "");
    }

    // Lo queremos devolver limpio en minusculas
    cadena = cadena.toLowerCase();

    // Quitamos espacios y los sustituimos por -
    cadena = cadena.replace(/ /g, "-");

    // Quitamos acentos y "ñ". Fijate en que va sin comillas el primer parametro
    cadena = cadena.replace(/á/gi, "a");
    cadena = cadena.replace(/é/gi, "e");
    cadena = cadena.replace(/í/gi, "i");
    cadena = cadena.replace(/ó/gi, "o");
    cadena = cadena.replace(/ú/gi, "u");
    cadena = cadena.replace(/ñ/gi, "n");
    return cadena;
}