#FROM node:10

# Create app directory
#WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
#COPY package*.json ./

#RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
#COPY . .

#CMD [ "node", "src/server.js" ]

FROM centos:7
USER root
RUN yum -y update
RUN yum install make -y
RUN yum install wget -y
RUN yum install curl -y
RUN yum install -y gcc-c++ make
RUN curl -sL https://rpm.nodesource.com/setup_10.x | bash -
RUN yum install nodejs -y
RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm
RUN yum install ./google-chrome-stable_current_*.rpm -y
WORKDIR /home/app
COPY package*.json ./
RUN npm install
COPY . .
RUN npx webpack

CMD [ "node", "src/server.js" ]