const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

/*module.exports = {
  entry: __dirname + '/src/widget.js',
  output:{
      path: __dirname + '/wid',
      filename: 'link.js'
  },
  mode: 'production',
}*/

module.exports = {
  entry: __dirname + '/src/public/main.js',
  output: {
    path: __dirname + '/build',
    filename: 'bundle.js'
  },
  mode: 'production',
  module: {
    rules: [
      {
        test: /\.css|\.s(c|a)ss$/,
        use: [{
          loader: 'polymer-css-loader',
          options: {
            minify: true,
          },
        }, 'extract-loader', 'css-loader', 'sass-loader'],
      },
      {
        test: /\.(png|woff|woff2|eot|ttf|svg)$/, loader: 'url-loader?limit=100000'
      }
    ],
  },
  resolve: {
    modules: [
      path.resolve(__dirname, 'node_modules')
    ]
  },
  plugins: [
    new CopyWebpackPlugin([
      {
        from: path.resolve(__dirname, 'node_modules/@polymer/polymer/*.js'),
        to: 'node_modules/@polymer/polymer/[name].[ext]'
      }
    ], {}),
  ]
}
